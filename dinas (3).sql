-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 21 Apr 2015 pada 19.22
-- Versi Server: 5.6.11
-- Versi PHP: 5.5.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `dinas`
--
CREATE DATABASE IF NOT EXISTS `dinas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dinas`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(150) NOT NULL,
  `tanggal` date NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `penulis` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL,
  `publish` enum('Y','N') NOT NULL DEFAULT 'Y',
  `counter` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id`, `judul`, `isi`, `gambar`, `tanggal`, `kategori`, `penulis`, `link`, `publish`, `counter`) VALUES
(13, 'SEMINAR NASIONAL', '<p>Kepala Dinas Sosial Kota Makassar Bapak Drs. H. Yunus Said, M.Si. Menjadi narasumber dalam Seminar Nasional yang diadakan oleh Universitas Negeri Makassar dengan tema "Perilaku Anrti Sosial Remaja: Kejahatan atau Kenakalan?" acara ini dihadiri oleh pembicara Bapak Prof.Dr. Sarlito Wirawan Sarwono. Ph.D Guru Besar Psikologi Universitas Indonesia.<br /><br />dalam seminar tersebut dihadiri oleh ratusan peserta dari berbagai universitas negeri maupu swasta.</p>', '5536621a7770f.jpg', '2015-04-21', '', 'admin', 'seminar-nasional.html', 'Y', 0),
(14, 'PASTIKAN DUA PROGRAM BAGI DIFABEL 2015', '<p>Dinas Sosial Kota Makassar sudah mempersiapkan dua program kerja khusus difabel di Kota Makassar. Dua program yang dimaksud&nbsp;<em>pertama,&nbsp;</em>&nbsp;Pembinaan dan Pengembangan Jaminan Sosial Disabilitas;&nbsp;<em>kedua,&nbsp;</em>Jaminan Sosial Bagi Perempuan Disabilitas. Keduanya telah disahkan dalam Rencana Pembangunan Jangka Menegah Daerah (RPJMD) dan akan dilaksanakan secara rutin dalam lima tahun ke depan. Rencana ini disampaikan Kepala Dinas Sosial Sosial, Drs. Andi Muh Yasir, M.Si , saat ditemui kontributor Solider Selasa.<br /><br /><br />Pada 2015 ini pemerintah telah menyiapkan anggaran 312 juta rupiah untuk program Pembinaan dan Pengembangan Jaminan Sosial Disabilitas dan 243 juta rupiah untuk Jaminan Sosial Bagi Perempuan Disabilitas. Jadi ada 555 juta rupiah yang Dinas Sosial anggarkan khusus difabel dari 17 miliar rupiah yang akan dikelola pada tahun ini.<br /><br />&ldquo;Yang perlu menjadi catatan bahwa Dinas Sosial tidak hanya mengurusi kelompok difabel, tapi ada begitu banyak sektor yang menjadi perhatian kami, misalnya di kebencanaan, jaminan sosial bagi rakyat miskin dan sebagainya,&rdquo;&nbsp;<br />Dinas Sosial juga saat ini sedang berusaha merampungkan pendataan difabel di Kota Makassar untuk memepersiapkan jaminan kesehatan difabel melalui skema Jaminan Kesehatan Nasional (JKN). Menurut keterangan Muhammad Yasir, ditargetkan sebelum pertengahan tahun data sudah terkumpul dan kelompok difabel sudah dapat merasakan manfaatnya.<br /><br />&ldquo;Kami menargetkan pendataan selesai pada tengah tahun, kalau perlu bulan depan sudah bisa diajukan ke otoritas JKN untuk segera ditindaklanjuti,&rdquo; kata Yasir. Ia menambahkan,&nbsp;&nbsp;&ldquo;Persoalan difabel ini tidak bisa diselesaikan oleh Dinas Sosial sendiri. Perlu ada keterlibatan SKPD terkait lainnya seperti Dinas Bina Marga, Dinas Kesehatan, Dinas Pendidikan dan sebagainya,&rdquo;</p>', '', '2015-04-21', '', 'admin', 'pastikan-dua-program-bagi-difabel-2015.html', 'Y', 0),
(15, 'ITSBAT NIKAH MASSAL KOTA MAKASSAR', '<p>Menghadapi fenomena penyandang masalah sosial mengenai sulitnya biaya pernikahan yang terjadi di kota Makassar, Dinas Sosial Kota Makassar mengadakan Itsbat Nikah Massal yang dilaksakan pada hari rabu dan kamis di aula SMK 04 Makassar. Dinas Sosial Kota Makassar melalui Hakim pengadilan Agama Kota Makassar mengitsbatkan 200 pasangan yang belum mempunyai buku nikah.<br /><br />Drs. Andi Muh Yasir, M.Si mengatakan Dinas Sosial yang memprakarsai Isbat ini&nbsp;<span class="text_exposed_show">bekerjasama dengan Lembaga Swadaya Internasional Australia Indonesia Partnership for Justice Program (AIPJ), bekerja sama dengan LSM Jaringan Advokasi Identitas Hukum Makassar,Kementrian Agama, Dinas Kependudukan dan Catatan Sipil, KUA Kecamatan, Pengadilan Agama, saling penopang demi kerlancaran prosesi itsbat nikah massal ini.<br /></span></p>\r\n<div class="text_exposed_show">pada pembukaan acara dibuka langsung oleh Sekretaris Daerah Kota Makassar,Bapak H.Ibrahim Shaleh yang mewakili Bapak Walikota Makassar.<br /><br />jika pada itsbat massal sebelumnya dilakukan hanya berupa pemberian buku nikah, namun kali ini dengan bekerja sama dengan Dinas Kependudukan dan Catatan Sipil, maka ditempat itu juga segera dibuatkan Akte Kelahiran untuk anak dari pasangan itsbat. dan semuanya tidak dipungut biaya, tentunya ini bisa menjadi tolak ukur untuk menjadikan SKPD saling menopang agar terwujudnya pelayanan publik yang standar dunia tentunya.<br /><br />perlu diketahui dari 500 kabupaten/kota di Indonesia baru 40 kabupaten/kota yang melakukan kegiatan seperti ini.<br /><br />Di Sulawesi Selatan baru dua kabupaten/kota yang melakukan isbat yaitu Kabupaten Bone, 50 pasangan. Dan Kota Makassar, 200 pasangan. Makassar adalah penyelenggara Itsbat Nikah Massal yang paling banyak antara Kabupaten/Kota. ungkap Bapak Kepala Dinas Sosial Kota Makassar.&nbsp;<a class="_58cn" href="https://www.facebook.com/hashtag/ar?source=feed_text&amp;story_id=1405800786377115" data-ft="{&quot;tn&quot;:&quot;*N&quot;,&quot;type&quot;:104}"><span class="_58cl">?#&lrm;</span><span class="_58cm">AR?</span></a></div>', '553668a96a5b1.jpg', '2015-04-21', '', 'admin', 'itsbat-nikah-massal-kota-makassar.html', 'Y', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspirasi`
--

CREATE TABLE IF NOT EXISTS `aspirasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(155) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `aspirasi` text NOT NULL,
  `nama` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tanggal` date NOT NULL,
  `blokir` enum('Y','N') NOT NULL DEFAULT 'N',
  `dukungan` int(11) NOT NULL,
  `tanggapan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

--
-- Dumping data untuk tabel `aspirasi`
--

INSERT INTO `aspirasi` (`id`, `id_session`, `ip`, `judul`, `aspirasi`, `nama`, `email`, `tanggal`, `blokir`, `dukungan`, `tanggapan`) VALUES
(137, 'PASasjd823y2uy3234', '::1', 'PASAR MALAM DAN ARAK ARAKAN', 'Setiap tahun diadakan arak2an dan pasar malam di sepanjang jl Sulawesi. Menurut panitia pasar malam, itu merupakan SK Walikota. Sudahkah sosialisasi dgn warga sekitar sebelum menutup akses jalan ? sdh pikirkan dampak kemacetan dan kebersihan lingkungan ? sdh pikirkan tentang dampak keributan dan kebisingan yg disebabkan hingga mengganggu warga sekitar ? Walikota punya hak apa utk menentukan kapan dan utk apa akses publik seperti jalanan di tutup ? apakah demi nilai ekonomis pasar malam itu ? demi kas daerah ? kapan mereka pernah sosialisasi atau berembuk dengan kami para warganya ? Ada yg berkata cuma setahun sekali, tapi fakta adalah hari libur dalam setahun kalender sudah nyaris menyamai hari kerja. Belum lagi keadaan yg serba sepi alias lesu perekonomian. Jadi permintaan warga adalah agar even itu dipindahkan ke ruang publik yg besar dan memadai tanpa menimbulkan dampak lingkungan merugikan bagi warga. Kamilah yang memilih Walikota dan Wakilnya. Jadi kami harap mereka mendengarkan kami.', 'deni', 'ss@gmail.com', '2015-03-31', 'N', 0, ''),
(138, 'ehu23iuy28', '::1', 'GENG MOTOR', 'Minta tolong pada pak Walikota,,, Tolong ini makassar diadakan pengamanan Super Ketat Selama seminggu ini di seluruh tempat makassar, yang rawan perampokan, penodongan , Geng Motor, dan pembunuhan warga yang melintas. Kami sebagai Warga Kota Makassar Merasa sudah tidak aman dan nyaman tinggal di makassar ini. sedikit2 nyawa melayang, kalau begini terus, susahki beraktifitas kasian, tolong ini geng motor disisir sampai ke akar2x,,janganmi dilepas klw adami ditangkap, jangmi lepas2ki itu klw ada didapat,, tidak nupikirki ini warga kasian dibunuh tas satu-satu. Minta tolong ini pak Walikota, dengan hormat dan sangat hormat. ndak kasianki itu liat ini yang kayk begini kasian, barusan selesai satu anak Jl.Racing,,ada lagi yang lain.', 'deden', 'lopisquenak@gmail.com', '2015-03-31', 'N', 0, ''),
(140, '551a57597fe84', '::1', 'USAHA BESI TUA DI JLN.SEPAKAT PETTRANI-MKS', 'Kepada Bpk.Walikota dan Bpk.Walikota yang terhormat, saya tinggal di Jalan sepakat, tepatnya di seputaran poros jalan Utama Pettarani Makassar, saya dan warga Jl.Sepakat sangat miris melihat aktivitas yang sudah berlangsung sangat lama di Ujung jalan kami, dimana hampir setiap hari aktifitas bongkar muat container besi Tua, ini sudah sangat mengganggu aktifitas kami warga di jalan sepakat, tumpukan besi tua, yang ada mempersempit jalanan yang tiap hari kami lewati, belum lagi banyaknya benda2 dari besi tua yang mengenai ban kendaraan kam.. Program Makassar&#039;ta tidak rantasa akan sulit terealisasi di pemukiman kami akibat besi tua yg menggunung hampir setiap harinya, aturan untuk mobil dengan muatan besar masuk ke dalam kotapun sepertinya tidakk diindahkan, container setiap hari bolak balik Penilaian Adipura akan segera dilakukan, pettarani merupakan jalan utama pasti tidak luput dari penilaian panitia Adipura, dengan kondisi seperti diatas sulit kota makassar bisa meraihnya Tolong hal ini bisa di tindak lanjuti, saya punya foto2 mengenai dampak dari usaha besi tua ini, sekali lagi agar keluhan kami bisa di tindaklanjuti Salam\n\nKepada Bpk.Walikota dan Bpk.Walikota yang terhormat, saya tinggal di Jalan sepakat, tepatnya di seputaran poros jalan Utama Pettarani Makassar, saya dan warga Jl.Sepakat sangat miris melihat aktivitas yang sudah berlangsung sangat lama di Ujung jalan kami, dimana hampir setiap hari aktifitas bongkar muat container besi Tua, ini sudah sangat mengganggu aktifitas kami warga di jalan sepakat, tumpukan besi tua, yang ada mempersempit jalanan yang tiap hari kami lewati, belum lagi banyaknya benda2 dari besi tua yang mengenai ban kendaraan kam.. Program Makassar&#039;ta tidak rantasa akan sulit terealisasi di pemukiman kami akibat besi tua yg menggunung hampir setiap harinya, aturan untuk mobil dengan muatan besar masuk ke dalam kotapun sepertinya tidakk diindahkan, container setiap hari bolak balik Penilaian Adipura akan segera dilakukan, pettarani merupakan jalan utama pasti tidak luput dari penilaian panitia Adipura, dengan kondisi seperti diatas sulit kota makassar bisa meraihnya Tolong hal ini bisa di tindak lanjuti, saya punya foto2 mengenai dampak dari usaha besi tua ini, sekali lagi agar keluhan kami bisa di tindaklanjuti Salam', 'deni', 'setiawan', '2015-03-31', 'N', 0, ''),
(143, '551a64b1ba0ed', '::1', 'LAYANAN MOBIL SAMPAH  Yth. Kepala Dinas Pertamanan dan Kebersihan', 'th. Kepala Dinas Pertamanan dan Kebersihan Kota Makassar Saya Syahlan alamat Bumi Permata Sudiang I Blok I.12 No. 3, kami mohon penjelasan kenapa rumah kami tidak dilayani oleh Mobil sampah, padahal tetangga sebelah saya koq dilayani? Apakah saya bukan warga kota Makassar? Padahal saya juga mau bayar berapapun tarifnya. ketika saya tanya tetangga, katanya penagihnya tidak mau kerumah saya karena saya pernah ngasi uang langsung ke sopir sampah, padahal sy hanya memberikan sumbangan sekedarnya, Mohon ditindak lanjuti.... Trimakasih', 'deni', 'deni', '2015-03-31', 'N', 0, ''),
(144, '551a65417cd92', '::1', 'LAYANAN MOBIL SAMPAH', 'Yth. Kepala Dinas Pertamanan dan Kebersihan Kota Makassar Saya Syahlan alamat Bumi Permata Sudiang I Blok I.12 No. 3, kami mohon penjelasan kenapa rumah kami tidak dilayani oleh Mobil sampah, padahal tetangga sebelah saya koq dilayani? Apakah saya bukan warga kota Makassar? Padahal saya juga mau bayar berapapun tarifnya. ketika saya tanya tetangga, katanya penagihnya tidak mau kerumah saya karena saya pernah ngasi uang langsung ke sopir sampah, padahal sy hanya memberikan sumbangan sekedarnya, Mohon ditindak lanjuti.... Trimakasih', 'deni', 'setiawna', '2015-03-31', 'N', 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `badge`
--

CREATE TABLE IF NOT EXISTS `badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(150) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `badge`
--

INSERT INTO `badge` (`id`, `nama_file`, `keterangan`) VALUES
(7, '551658e4e7c6c.jpg', 'logo i love mc'),
(8, '551658ecb7c20.jpg', 'mc'),
(9, '55165926c159c.png', 'makassar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `download`
--

CREATE TABLE IF NOT EXISTS `download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_halaman` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `nama_file` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data untuk tabel `download`
--

INSERT INTO `download` (`id`, `id_halaman`, `judul`, `nama_file`) VALUES
(19, 240317263, 'Perpres Tentang Kesehatan', '55109839e2f16.pdf'),
(20, 240317263, 'UU Terkait Dinas Kesehatan', '5510999283fb3.pdf'),
(21, 27036289, '123213', '551573528a45e.pdf'),
(22, 27036289, '123213', '5515738bab811.pdf'),
(23, 27036289, '434343', '55157398b94e5.pdf'),
(24, 27039177, 'jji', '5515e3803c8d7.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dukungan`
--

CREATE TABLE IF NOT EXISTS `dukungan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_aspirasi` int(11) NOT NULL,
  `id_session` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `konten` text NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `longitude` varchar(75) NOT NULL,
  `latitude` varchar(75) NOT NULL,
  `gambar` varchar(150) NOT NULL,
  `penulis` varchar(150) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data untuk tabel `event`
--

INSERT INTO `event` (`id`, `judul`, `konten`, `tanggal_mulai`, `tanggal_selesai`, `tempat`, `longitude`, `latitude`, `gambar`, `penulis`, `link`) VALUES
(1, 'Kunjungan menteri kesehatan', '<p>Aenean vitae lorem aliquet, tempor ipsum vel, convallis massa. Aliquam laoreet sapien at ante pharetra aliquet. Pellentesque pulvinar luctus lacinia. Vivamus et blandit risus, sit amet interdum arcu. Pellentesque blandit in justo maximus finibus. Nulla facilisi. Vestibulum finibus nisi eget tortor pretium dignissim. Morbi pharetra imperdiet dui ac accumsan. Fusce pellentesque placerat ligula quis luctus. Fusce bibendum mi nec pharetra pretium. Nam vel viverra ex. Nunc pellentesque diam eu lectus pretium mollis.</p>', '2015-03-31', '2015-03-19', 'kampus unhas', '-5.131301835847433', '-5.131301835847433', '', 'admin', 'Kunjungan-menteri-kesehatan.html'),
(7, 'Dinas Kesehatan to School', '<p>Nullam eu lorem odio. In nec mattis purus. Nulla non sagittis augue, nec aliquam enim. Sed tincidunt at diam sed dapibus. Suspendisse id interdum nunc. Nunc non iaculis risus, at aliquam tellus. Cras porttitor ultricies diam, quis efficitur nisl.</p>', '2015-03-29', '2015-03-19', 'jl pettarani', '119.41494932015985', '-5.131301835847433', '', 'admin', 'Dinas-Kesehatan-to-School.html'),
(18, 'peresmian rumah sakit di tidung ', '<p>peresmian ini akan di buka oleh walikota makassar</p>', '2015-03-28', '2015-03-28', 'jl tidung', '119.41494932015985', '-5.131301835847433', '', 'admin', 'peresmian-rumah-sakit-di-tidung.html'),
(19, 'Rapat terkait Smart City', '<p>Rapat terkait Smart City, Dinas Sosial Kota Makassar sudah membuat aksi untuk program Smart City</p>', '2015-04-27', '2015-04-30', 'Kantor Dinas Sosial', '119.407761', '-5.133439', '', 'admin', 'rapat-terkait-smart-city.html'),
(20, 'SEMINAR NASIONAL  bersama Guru Besar Psikologi Universitas Indonesia ', '', '2015-04-28', '2015-04-30', 'Hotel Clarion Makassar', '119.407761', '-5.133439', '', 'admin', 'seminar-nasional--bersama-guru-besar-psikologi-universitas-indonesia.html'),
(21, 'Rapat Lembaga Konsultasi Kesejahteraan Keluarga (LK3) ', '', '2015-04-21', '2015-04-21', 'Kantor Dinas Sosial', '119.407761', '-5.133439', '', 'admin', 'rapat-lembaga-konsultasi-kesejahteraan-keluarga-(lk3).html');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL,
  `tanggal` date NOT NULL,
  `link` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id`, `nama`, `tanggal`, `link`) VALUES
(12, 'GAMBAR UTAMA', '2015-04-01', 'gambar-utama.html'),
(13, 'Berkebun di tengah kota', '2015-04-22', 'berkebun-di-tengah-kota.html'),
(14, 'Aksi anak muda dalam memberdayakan warga Eks Kusta Dangko', '2015-04-22', 'aksi-anak-muda-dalam-memberdayakan-warga-eks-kusta-dangko.html'),
(15, 'Rapat dengan tim pendamping SKPD terkait program smart city', '2015-04-22', 'rapat-dengan-tim-pendamping-skpd-terkait-program-smart-city.html');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri_detail`
--

CREATE TABLE IF NOT EXISTS `galeri_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_galeri` int(11) NOT NULL,
  `nama_file` varchar(150) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data untuk tabel `galeri_detail`
--

INSERT INTO `galeri_detail` (`id`, `id_galeri`, `nama_file`, `keterangan`) VALUES
(2, 6, '550d843b9adab.jpg', 'percobaan'),
(3, 7, '55129496d60ae.jpg', 'Peresmian Rumah Sakit'),
(5, 8, '5512966360210.jpg', 'oleh dr sakini'),
(6, 8, '5512969168ebe.jpg', 'peserta pembinaan'),
(9, 9, '5512a4c54727b.jpg', 'dari dokter indah'),
(10, 9, '5512a4cba228c.jpg', 'dari dokter indah'),
(14, 12, '553676c7dc647.png', 'Penyematan Baju "SARIBATTANG"'),
(15, 12, '553676e9f4016.jpg', 'Mobil Pengangkut Sampah Kota Makassar'),
(16, 12, '5536770eb9a92.jpg', 'Penertiban dan Pembinaan Penyandang Masalah Kesejahteraan Sosial'),
(18, 12, '553677dea3d2f.jpg', 'Rapat dengan tim pendamping'),
(19, 13, '55367cc3b0ad4.jpg', 'Aseknya...berkebun di tengah kota, manfaakan ruang sempit kota dgn fresh produktif di weekend @kebun wanita Manggala '),
(20, 13, '55367d3f9b574.jpg', 'Aseknya...berkebun di tengah kota, manfaakan ruang sempit kota dgn fresh produktif di weekend @kebun wanita Manggala '),
(22, 14, '55367e53c23da.jpg', 'aksi anak mudaMKS dlm memberdayakan warga Eks Kusta Dangko'),
(23, 14, '55367e6e2c28b.jpg', 'aksi anak mudaMKS dlm memberdayakan warga Eks Kusta Dangko'),
(24, 15, '55367f14714a7.png', 'Rapat dengan tim pendamping SKPD terkait program smart city'),
(25, 15, '55367f801cf34.jpg', 'Rapat Koordinasi LK3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `halaman`
--

CREATE TABLE IF NOT EXISTS `halaman` (
  `id_halaman` int(11) NOT NULL,
  `judul_halaman` varchar(255) NOT NULL,
  `parrent_halaman` int(11) NOT NULL,
  `konten_halaman` text NOT NULL,
  `link_halaman` varchar(255) NOT NULL,
  `publish` enum('Y','N') NOT NULL DEFAULT 'Y',
  `atribut` int(2) NOT NULL DEFAULT '0' COMMENT 'kalo 1 artinya dia itu default',
  `gambar_halaman` varchar(255) NOT NULL,
  PRIMARY KEY (`id_halaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `halaman`
--

INSERT INTO `halaman` (`id_halaman`, `judul_halaman`, `parrent_halaman`, `konten_halaman`, `link_halaman`, `publish`, `atribut`, `gambar_halaman`) VALUES
(31031, 'KONTAK', 1, '', 'kontak.html', 'Y', 1, ''),
(31086, 'STRUKTUR-ORGANISASI', 0, '', 'struktur-organisasi.html', 'Y', 1, ''),
(240317, 'DASAR HUKUM', 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies orci et sapien pharetra, in euismod odio rutrum. Vivamus eget hendrerit augue. Mauris nec condimentum dolor. Etiam pharetra tellus quam, sit amet semper neque ultrices congue.</p>\r\n<p>&nbsp;</p>', 'dasar-hukum.html', 'Y', 0, ''),
(250391, 'UPTD 1', 0, '<h4>LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. MORBI ULTRICIES ORCI ET SAPIEN PHARETRA, IN EUISMOD ODIO.LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT.</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies orci et sapien pharetra, in euismod odio rutrum. Vivamus eget hendrerit augue. Mauris nec condimentum dolor. Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh. Duis feugiat massa interdum felis dignissim congue vitae sit amet lorem. Aliquam porta tincidunt pharetra. Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</p>\r\n<p>Praesent dictum congue justo, sed tincidunt nisi facilisis vel. Nulla vestibulum nibh quis ante posuere consectetur. Ut mattis sapien eu orci laoreet ultricies. Aliquam erat volutpat. Suspendisse sed augue sed leo ultricies pulvinar in quis arcu. Integer scelerisque, nisi at ultrices porta, lectus libero sollicitudin risus, et consequat augue odio eget tortor.</p>\r\n<p>&nbsp;</p>\r\n<h4>VESTIBULUM LUCTUS EU MAGNA EU MATTIS.</h4>\r\n<ul>\r\n<li>Integer et ligula hendrerit, varius orci ac, ultrices enim. Mauris cursus cursus luctus.</li>\r\n<li>Morbi ultricies orci</li>\r\n<li>Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat.</li>\r\n<li>Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh.</li>\r\n<li>Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</li>\r\n<li>Vestibulum venenatis</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>Vestibulum luctus eu magna eu mattis. Cras dui eros, tempus sit amet nisi a, ullamcorper pretium turpis. Nullam pellentesque ante ac turpis efficitur semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies orci et sapien pharetra, in euismod odio rutrum. Vivamus eget hendrerit augue. Mauris nec condimentum dolor. Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh. Duis feugiat massa interdum felis dignissim congue vitae sit amet lorem. Aliquam porta tincidunt pharetra. Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</p>\r\n<p>&nbsp;</p>\r\n<h4>VESTIBULUM LUCTUS EU MAGNA EU MATTIS.</h4>\r\n<ol>\r\n<li>Integer et ligula hendrerit, varius orci ac, ultrices enim. Mauris cursus cursus luctus.</li>\r\n<li>Morbi ultricies orci</li>\r\n<li>Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat.</li>\r\n<li>Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh.</li>\r\n<li>Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</li>\r\n<li>Vestibulum venenatis</li>\r\n</ol>\r\n<p>Integer et ligula hendrerit, varius orci ac, ultrices enim. Mauris cursus cursus luctus. Vestibulum venenatis quam eget justo porttitor scelerisque. Fusce nibh augue, suscipit eget aliquet quis, hendrerit id nisl.</p>', 'uptd-1.html', 'Y', 0, ''),
(364212, 'VISI DAN MISI', 1, '<div class="\\">\r\n<h3>Visi dan Misi Dinas Sosial Kota Makassar</h3>\r\n<p><br />Berdasarkan tugas pokok dan fungsi Dinas Sosial, Maka Visi Dinas Sosial Kota Makassar adalah sebagai berikut :<br />&middot; Pengendalian permasalahan sosial berbasis masyarakat tahun 2014<br /> Maknanyan adalah manusia membutuhkan kepercayaan diri yang dilandasi oleh nilai-nilai kultur lokal yang diarahkan kepada aspek tatanan kehidupan dan penghidupan untuk menciptakan kemandirian lokal sebagai upaya pemenuhan kebutuhan dasar, peningkatan keterampilan kerja, ketentraman, kedamaian, dan keadilan sosial bagi dirinya sendiri, keluarga dan lingkungan sosial masyarakatnya, serta mendorong tingkat partisipasi sosial masyarakat dalam ikut melaksanakan proses pelayanan kesejahteraan sosial masyarakat.</p>\r\n<h3>Misi Dinas Sosial Sebagai berikut :</h3>\r\n<p>1. Meningkatkan partisipasi sosial masyarakat melalui pendekatan kemitraan dan pemberdayaan sosial masyarakat dengan semangat kesetiakawanan sosial masyarakat<br />2. Memperkuat ketahan sosial dalam mewujudkan keadilan sosial melalui upaya memperkecil kesenjangan sosial denagn memberikan pehatian kepada warga masyarakat yang rentan dan tidak beruntung<br />3. Mengembangkan sistem perlindungan sosial <br />4. Melakukan jaminan sosial<br />5. Pelayanan rehabilitasi sosial secara optimal<br />6. Mengembangkan pemberdayaan sosial.</p>\r\n</div>', 'visi-dan-misi.html', 'Y', 0, ''),
(2354885, 'SEJARAH SINGKAT', 1, '<h3>Dinas Sosial Kota Makassar</h3>\r\n<p>Dinas Sosial Kota Makassar yang sebelumnya adalah Kantor Departemen Sosial Kota Makassar didirikan berdasarkan Keputusan Presiden No. 44 Tahun 1974 Tentang Susunan Organisasi Departemen beserta lampiran-lampirannya sebagaimana beberapa kali dirubah, terakhir dengan Keputusan Presiden No. 49 Tahun 1983.</p>\r\n<p>Khusus di Indonesia Timur didirikan Departemen Sosial Daerah Sulawesi Selatan yang kemudian berubah menjadi Jawatan Sosial lalu dirubah lagi menjadi kantor Departemen Sosial berdasarkan keputusan Menteri Sosial RI No. 16 Tahun 1984 tentang Organisasi dan Tata Kerja Kantor Departemen Sosial di Propinsi maupun di kabupaten/Kotamadya. Dan akhirnya menjadi Dinas Sosial Kota Makassar pada tanggal 10 April 2000 yang ditandai dengan pengangkatan dan pelantikan Kepala Dinas Sosial Kota Makassar berdasarkan Keputusan Walikota Makassar, Nomor: 821.22:24.2000 tanggal 8 Maret 2000.</p>\r\n<p>Dinas Sosial Kota Makassar terletak di Jalan Arif Rahman Hakim No. 50 Makassar, Kelurahan Ujung pandang Baru, kecamatan Tallo Kota Makassar, berada pada tanah seluas 499m2, dengan bangunan fisik gedung berlantai 2 dan berbatasan dengan :</p>\r\n<p>1. Sebelah Utara berbatasan denagn Kantor Kecamatan Tallo Kota Makassar<br />2. Sebelah Selatan berbatasan dengan Perumahan Rakyat<br />3. Sebelah Barat berbatasan dengan Jalan Ujung Pandang Baru<br />4. Sebelah Timur berbatasan dengan Perumahan Rakyat</p>', 'sejarah-singkat.html', 'Y', 0, '551058ec819d9.jpg'),
(2503151, 'UPTD', 0, '<p>dinas ini membawahi Uptd .....</p>', 'uptd.html', 'Y', 0, ''),
(10411211, 'ASPIRASI', 1, '', 'aspirasi.html', 'Y', 1, ''),
(21042115, 'TUGAS POKOK', 1, '<p>Tugas Pokok<br />1. Kepala Dinas<br />Dinas Sosial Kota Makassar mempunyai tugas pokok yaitu melaksanakan sebagian tugas pokok sesuai kebijakan walikota dan peraturan perundang-undangan yang berlaku, merumuskan kebijaksanaan, mengoordinasikan, dan mengendalikan tugas-tugas dinas.<br />Dalam melaksanakan tugas sebagaimana pada point 1, Kepala Dinas menyelenggarakan fungsi :</p>\r\n<p>a. Perumusan kebijakan teknis dibidang usaha kesejahteraan sosial, yang meliputi partisipan sosial masyarakat, perlindungan sosial, jaminan sosial, rehabilitasi sosial dan pemberdayaan sosial, serta pembinaan organisasi sosial.<br />b. Perencanaan program di bidang usaha kesejahteraan sosial, yang meliputi partisipan sosial masyarakat, perlindungan sosial, jaminan sosial, rehabilitasi sosial dan pemberdayaan sosial, serta pembinaan organisasi sosial.<br />c. Pembinaan pemberian perizinan dan pelayanan umum di bidang usaha kesejahteraan sosial, yang meliputi perlindungan sosial, jaminan sosial, rehabilitasi sosial dan pemberdayaan sosial, serta pembinaan organisasi sosial.<br />d. Pengendalian dan pengamanan teknis oprerasional di bidang usaha kesejahteraan sosial, jaminan sosial, rehabilitasi sosial dan pemberdayaan sosial serta bimbingan organisasi sosial<br />e. Melakukan pembinaan Unit Pelaksanaan Teknis Dinas (UPTD)</p>\r\n<p>2. Sekretaris<br />Sekretaris mempunyai tugas pemberian, pelayanan administrasi bagi seluruh satuan kerja di lingkup Dinas Sosial Kota Makassar.</p>\r\n<p>a. Sub Bagian Umum dan Kepegawaian<br />Sub Bagian umum dan Kepegawaian mempunyai tugas menyusun rencana kerja, melaksanakan tugas teknis ketatausahaan, mengelola administrasi kepegawaian serta melaksanakan urusan kerumah tanggaan dinas.</p>\r\n<p>b. Sub Bagian Keuangan<br />Sub Bagian Keuangan mempunyai tugas menuyusun rencana kerja, melaksanakan tugas teknis keuangan.</p>\r\n<p>c. Sub Bagian Perlengkapan<br />Sub Bagian Perlengkapan mempunyai tugas menyusun rencana kerja, melaksanakan tugas teknis perlengkapan, membuat laporan serta mengevaluasi semua pengadaan barang.</p>\r\n<p>&nbsp;</p>\r\n<p>3. Bidang Usaha Kesejahteraan Sosia<br />Bidang Usaha Kesejahteraan Sosial mempunyai tugas melaksanakan pembinaan, kegiatan dibidang penyuluhan dan bimbingan sosial, pembinaan keluarga penyandang masalah kesejahteraaan sosial (PMKS) dan potensi sumber kesehajteraan sosial (PSKS), pembinaan karang taruna dan pelaksanaan penelitian/ pendataan PMKS dan PSKS.</p>\r\n<p>4. Bidang Rehabilitasi Sosial<br />Bidang Rehabilitasi Sosial mempunyai tugas melaksanakan rehabilitasi sosial penyandang cacat, rehabilitasi tuna sosial, dan pembinaan anak jalanan, gelandangan, pengemis dan pengamen, korban tindak kekerasan pekerja migran.</p>\r\n<p>5. Bidang Pengendalian Bantuan dan Jaminan Kesejahteraan Sosial<br />Bidang pengendalian Bantuan dan Jaminan Kesejahteraan Sosial mempunyai tugas melaksanakan kegiatan pengendalian bantuan, pemberian bantuan dan jaminan kesejahteraan sosial termasuk pengendalian daearh rawan bencana dan daerah kumuh, bantuan kepada masyarakat fakir miskin serta bantuan kepada korban bencana alam dan sosial serta pelayanan kepada orang terlantar.</p>\r\n<p>6. Bidang Bimbingan Organisasi Sosial<br />Bidang Bimbingan Organisasi Sosial mempunyai tugas melaksakana bimbingan dan pelayanan terhadap organisasi sosial/LSM dan anak terlantar, pengendalian dan penertiban usaha pengumpulan sumbangan sosial dan undian berhadiah serta melaksanakan pembinaan dan pemahaman pelestarian nilai kepahlawanan, keperintisan dan kejuangan serta kesetiakawanan.</p>', 'tugas-pokok.html', 'Y', 0, ''),
(21042164, 'STRUKTUR ', 1, '', 'struktur.html', 'Y', 0, '553681d12c0bf.jpg'),
(22044282, 'BIDANG', 1, '', 'bidang.html', 'Y', 0, ''),
(22047208, 'USAHA KESEJAHTERAAN SOSIAL', 1, '<p>Bidang Usaha Kesejahteraan Sosial mempunyai tugas melaksanakan pembinaan, kegiatan dibidang penyuluhan dan bimbingan sosial, pembinaan keluarga penyandang masalah kesejahteraaan sosial (PMKS) dan potensi sumber kesehajteraan sosial (PSKS), pembinaan karang taruna dan pelaksanaan penelitian/ pendataan PMKS dan PSKS.</p>', 'usaha-kesejahteraan-sosial.html', 'Y', 0, ''),
(24032295, 'TUPOKSI', 1, '', 'tupoksi.html', 'Y', 0, ''),
(25039131, 'UPTD 2', 0, '<h4>LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. MORBI ULTRICIES ORCI ET SAPIEN PHARETRA, IN EUISMOD ODIO.LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT.</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies orci et sapien pharetra, in euismod odio rutrum. Vivamus eget hendrerit augue. Mauris nec condimentum dolor. Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh. Duis feugiat massa interdum felis dignissim congue vitae sit amet lorem. Aliquam porta tincidunt pharetra. Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</p>\r\n<p>Praesent dictum congue justo, sed tincidunt nisi facilisis vel. Nulla vestibulum nibh quis ante posuere consectetur. Ut mattis sapien eu orci laoreet ultricies. Aliquam erat volutpat. Suspendisse sed augue sed leo ultricies pulvinar in quis arcu. Integer scelerisque, nisi at ultrices porta, lectus libero sollicitudin risus, et consequat augue odio eget tortor.</p>\r\n<h4>&nbsp;</h4>\r\n<h4>VESTIBULUM LUCTUS EU MAGNA EU MATTIS.</h4>\r\n<ul>\r\n<li>Integer et ligula hendrerit, varius orci ac, ultrices enim. Mauris cursus cursus luctus.</li>\r\n<li>Morbi ultricies orci</li>\r\n<li>Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat.</li>\r\n<li>Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh.</li>\r\n<li>Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</li>\r\n<li>Vestibulum venenatis</li>\r\n</ul>\r\n<h4>&nbsp;</h4>\r\n<p>Vestibulum luctus eu magna eu mattis. Cras dui eros, tempus sit amet nisi a, ullamcorper pretium turpis. Nullam pellentesque ante ac turpis efficitur semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies orci et sapien pharetra, in euismod odio rutrum. Vivamus eget hendrerit augue. Mauris nec condimentum dolor. Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh. Duis feugiat massa interdum felis dignissim congue vitae sit amet lorem. Aliquam porta tincidunt pharetra. Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</p>\r\n<h4>&nbsp;</h4>\r\n<h4>VESTIBULUM LUCTUS EU MAGNA EU MATTIS.</h4>\r\n<ol>\r\n<li>Integer et ligula hendrerit, varius orci ac, ultrices enim. Mauris cursus cursus luctus.</li>\r\n<li>Morbi ultricies orci</li>\r\n<li>Etiam pharetra tellus quam, sit amet semper neque ultrices congue. Maecenas quis odio ut augue lacinia consequat.</li>\r\n<li>Maecenas quis odio ut augue lacinia consequat. Curabitur eget purus vel nibh viverra porta non non nibh.</li>\r\n<li>Phasellus quam mauris, sodales ac congue ac, pretium vitae lorem.</li>\r\n<li>Vestibulum venenatis</li>\r\n</ol>\r\n<p>Integer et ligula hendrerit, varius orci ac, ultrices enim. Mauris cursus cursus luctus. Vestibulum venenatis quam eget justo porttitor scelerisque. Fusce nibh augue, suscipit eget aliquet quis, hendrerit id nisl.</p>', 'uptd-2.html', 'Y', 0, ''),
(220410249, 'BIDANG KEWENANGAN', 1, '<p>1. Perencanaan pembangunan kesejahteraan sosial wilayah kabupaten / kota dan pendataan penyandang masalah kesejahteraan sosial<br />2. Penyuluhan dan bimbingan sosial<br />3. Pembinaan nilai kepahlawanan, keperintisan dan kejuangan<br />4. Pelayanan kesejahteraan sosial lanjut usia terlantar (dalam dan luar panti)<br />5. Pelayanan kesejahteraan sosial anak balita melalui penitipan anak dan adopsi lingkup kabupaten / kota<br />6. Pelayanan anak terlantar, anak cacat dan anak nakal (dalam dan luar panti)<br />7. Pelayanan dan rehabilitasi sosial penderita cacat<br />8. Pelayanan dan rehabilitasi sosial tuna sosial ( tuna susila, gelandangan, pengemis, dan eks narapidana )<br />9. Pemberdayaan keluarga fakir miskin meliputi fakir miskin, komunitas adat terpencil dan wanita rawan sosial ekonomi<br />10. Pemberdayaan karang taruna / organisasi kepemudaan<br />11. Pemberdayaan organisasi sosial / LSM lingkup kabupaten / kota<br />12. Pemberdayaan tenaga kerja sosial masyarakat<br />13. Pemberdayaan dunia usaha(partisipasi dalam usaha kesejahteraan sosial)<br />14. Pemberdayaan pengumpulan sumbangan sosial lingkup kabupaten/kota<br />15. Penanggulangan korban bencana alam lingkup kabupaten/kota<br />16. Penanggulangan korban tindak kekerasan (anak, wanita dan lanjut usia)<br />17. Penanggulangan korban napza<br />18. Pelayanan kesejahteraan sosial keluarga<br />19. Pelayanan kesejahteraan angkatan kerja<br />20. Penelitian dan uji coba pengambangan usaha kesejahteraan sosial lingkup kabupaten/kota. Penyelenggaraan sistem informasi kesejahteraan sosial lingkup kabupaten/kota.<br />21. Penyelenggaraan pelatihan tenaga bidang usaha kesejahteraan sosial lingkup kabupaten/kota<br />22. Penyelenggaraan koordinasi pelaksanaan usaha kesejahteraan sosial lingkup kabupaten / kota<br />23. Monitoring, evaluasi dan pelaporan hasil pelaksanaan pelayanan kesejahteraan sosial.<br />Adapun sasaran dari bidang Kewenangan tersebut adalah Penyandang Masalah Kesejahteraan Sosial (PMKS), meliputi :</p>\r\n<p><strong>1. Anak Balita Terlantar</strong></p>\r\n<p><br />Permasalahan pokok yang berkaitan dengan anak balita terlantar antara lain kondisi gizi yang buruk, keterbatasan jangkauan pelayanan sosial bagi anak balita, disamping itu semakin terbatasnya waktu kedua orang tua untuk memberikan perhatian penuh bagi keberlangsungan tumbuh kembangnya anak dalam lingkungan keluarganya.</p>\r\n<p><strong>2. Anak terlantar</strong></p>\r\n<p><br />Pelayanan sosial yang diberikan kepada anak terlantar yaitu pemberdayaan anak terlantar melalui pemberian bantuan usaha ekonomis produktif dan kelompok usaha bersama serta pemberian latihan keterampilan melalui panti sosial bina remaja.</p>\r\n<p><strong>3. Anak Nakal</strong></p>\r\n<p><br />Pelayanan sosial yang diberikan terhadap anak nakal yaitu melalui pembinaan dalam panti yang dilaksanakan di Panti Marsudi Putra Salodong.</p>\r\n<p>4. Anank Jalanan</p>\r\n<p><br />Pelayanan Sosial yang diberikan kepada anak jalanan berupa pemberian beasiswa bagi anak jalanan usia sekolah, pemberian latihan keterampilan dan praktek kerja bagi anak jalanan serta pemberdayaan keluarga anak jalanan.</p>\r\n<p>5. Penjaja seks Komersial (PSK)</p>\r\n<p><br />Penanganan terhadap PSK ynag terjaring melalui razia diberikan pembinaan melalui panti dan non panti. Pembinaan dalam panti berupa pemberian latihan keterampilan yang dilaksanakan di Panti Sosial karya wanita mattiro deceng. Sedangkan pembinaan luar panti melalui kegiatan pemberdayaan berupa pemberian latihan keterampilan.</p>\r\n<p>&nbsp;</p>\r\n<p>6. Gelandangan Pengemis</p>\r\n<p><br />Penanganan yang telah dilaksanakan oleh Dinas Sosial yaitu melakukan pengawasan dan penertiban terhadap gepeng serta pemberdayaan gepeng beserta keluarganya melalui pemberian bantuan modal usaha.</p>\r\n<p>7. Eks korban penyalahgunaan napza</p>\r\n<p><br />Sesorang yang pernah menggunakan narkotika, psikotropika dan zat-zat adiktif lainnya termasuk minuman keras di luar tujuan pengobatan atau tanpa sepengetahuan dokter yang berwenang.</p>\r\n<p>8. Anak, wanita dan lanjut usia korban tindak kekerasan</p>\r\n<p><br />Anak berusia 5-18 tahun, wanita yang berusia 18-59 tahun dan lanjut usia yang berusia 60 tahun keatas yang terancam secara fisik atau non fisik (psikologis) yang mengalami tindak kekerasan, diperlakukan salah satu atau tidak semestinya dalam lingkungan keluarga atau lingkungan sosial terdekatnya.</p>\r\n<p>9. Penyandang cacat</p>\r\n<p><br />Pelayanan sosial yang diberikan bagi penyandang cacat adalah pemberian bantuan dana jaminan sosial bagi penyandang cacat berat melalui Departemen Sosial RI.</p>\r\n<p>10. Eks Kusta</p>\r\n<p><br />Eks kusta adalah sesorang yang pernah menderita penyakit kusta dan telah dinyatakan sembuh secara medis, tetapi mengalami hambatan untuk melaksanakan kegiatan sehari-hari karena dikucilkan keluarga atau masyarakat. Penanganan yang diberikan bagi eks kusta adalah pembedayaan keluarga eks kusta.</p>\r\n<p>11. Eks Narapidana</p>\r\n<p><br />Eks narapidana adalah seseorang yang telah selesai masa hukuman atau masa pidananya sesuai dengan keputusan pengadilan dan mengalami hambatan untuk menyesuaikan diri kembali dalam kehidupan masyarakat sehingga mendapat kesulitan untuk mendapatkan kehidupannya secara normal.</p>\r\n<p>12. Lanjut Usia terlantar</p>\r\n<p><br />Penanganan terhadap lanjut usia terlantar yang masih produktif yaitu pemberdayaan lanjut usia melalui pemberian bantuan usaha ekonomis produktif dan kelompok usaha bersama. Selain itu ada juga pemberian bantuan pelayanan dan jaminan sosial lanjut usia terlantar yang berasal dari Departemen Sosial RI.</p>\r\n<p>13. Wanita Rawan Sosial Ekonomi</p>\r\n<p><br />Wanita rawan sosial ekonomi adalah seorang wanita dewas berusia 18-59 tahun belum menikah atau janda dan tidak mempunyai penghasilan cukup untuk memenuhi kebutuhan pokok sehari-hari.</p>\r\n<p>14. Keluarga Fakir Miskin</p>\r\n<p><br />Pelayanan sosial yang diberikan bagi keluarga fakir miskin yaitu pengembangan potensi keluarga fakir miskin, pemberian latihan keterampilan berusaha bagi keluarga fakir miskin, pendampingan UEP dan KUBE fakir miskin.</p>\r\n<p>15. Keluarga berumah tidak layak huni</p>\r\n<p><br />Pelayanan sosial yang diberikan adalah rehablitasi rumah tidak layak huni berupa pemberian bantuan bahan bangunan rumah seperti seng, balok kayu, tripleks dan papan.</p>', 'bidang-kewenangan.html', 'Y', 0, ''),
(220411145, 'KEWENANGAN DINAS SOSIAL', 1, '<p>1. Perencanaan pembangunan kesejahteraan sosial wilayah kabupaten / kota dan pendataan penyandang masalah kesejahteraan sosial<br />2. Penyuluhan dan bimbingan sosial<br />3. Pembinaan nilai kepahlawanan, keperintisan dan kejuangan<br />4. Pelayanan kesejahteraan sosial lanjut usia terlantar (dalam dan luar panti)<br />5. Pelayanan kesejahteraan sosial anak balita melalui penitipan anak dan adopsi lingkup kabupaten / kota<br />6. Pelayanan anak terlantar, anak cacat dan anak nakal (dalam dan luar panti)<br />7. Pelayanan dan rehabilitasi sosial penderita cacat<br />8. Pelayanan dan rehabilitasi sosial tuna sosial ( tuna susila, gelandangan, pengemis, dan eks narapidana )<br />9. Pemberdayaan keluarga fakir miskin meliputi fakir miskin, komunitas adat terpencil dan wanita rawan sosial ekonomi<br />10. Pemberdayaan karang taruna / organisasi kepemudaan<br />11. Pemberdayaan organisasi sosial / LSM lingkup kabupaten / kota<br />12. Pemberdayaan tenaga kerja sosial masyarakat<br />13. Pemberdayaan dunia usaha(partisipasi dalam usaha kesejahteraan sosial)<br />14. Pemberdayaan pengumpulan sumbangan sosial lingkup kabupaten/kota<br />15. Penanggulangan korban bencana alam lingkup kabupaten/kota<br />16. Penanggulangan korban tindak kekerasan (anak, wanita dan lanjut usia)<br />17. Penanggulangan korban napza<br />18. Pelayanan kesejahteraan sosial keluarga<br />19. Pelayanan kesejahteraan angkatan kerja<br />20. Penelitian dan uji coba pengambangan usaha kesejahteraan sosial lingkup kabupaten/kota. Penyelenggaraan sistem informasi kesejahteraan sosial lingkup kabupaten/kota.<br />21. Penyelenggaraan pelatihan tenaga bidang usaha kesejahteraan sosial lingkup kabupaten/kota<br />22. Penyelenggaraan koordinasi pelaksanaan usaha kesejahteraan sosial lingkup kabupaten / kota<br />23. Monitoring, evaluasi dan pelaporan hasil pelaksanaan pelayanan kesejahteraan sosial.<br />Adapun sasaran dari bidang Kewenangan tersebut adalah Penyandang Masalah Kesejahteraan Sosial (PMKS), meliputi :<br />1. Anak Balita Terlantar<br />Permasalahan pokok yang berkaitan dengan anak balita terlantar antara lain kondisi gizi yang buruk, keterbatasan jangkauan pelayanan sosial bagi anak balita, disamping itu semakin terbatasnya waktu kedua orang tua untuk memberikan perhatian penuh bagi keberlangsungan tumbuh kembangnya anak dalam lingkungan keluarganya.<br />2. Anak terlantar<br />Pelayanan sosial yang diberikan kepada anak terlantar yaitu pemberdayaan anak terlantar melalui pemberian bantuan usaha ekonomis produktif dan kelompok usaha bersama serta pemberian latihan keterampilan melalui panti sosial bina remaja.<br />3. Anak Nakal<br />Pelayanan sosial yang diberikan terhadap anak nakal yaitu melalui pembinaan dalam panti yang dilaksanakan di Panti Marsudi Putra Salodong.<br />4. Anank Jalanan<br />Pelayanan Sosial yang diberikan kepada anak jalanan berupa pemberian beasiswa bagi anak jalanan usia sekolah, pemberian latihan keterampilan dan praktek kerja bagi anak jalanan serta pemberdayaan keluarga anak jalanan.<br />5. Penjaja seks Komersial (PSK)<br />Penanganan terhadap PSK ynag terjaring melalui razia diberikan pembinaan melalui panti dan non panti. Pembinaan dalam panti berupa pemberian latihan keterampilan yang dilaksanakan di Panti Sosial karya wanita mattiro deceng. Sedangkan pembinaan luar panti melalui kegiatan pemberdayaan berupa pemberian latihan keterampilan.<br />6. Gelandangan Pengemis<br />Penanganan yang telah dilaksanakan oleh Dinas Sosial yaitu melakukan pengawasan dan penertiban terhadap gepeng serta pemberdayaan gepeng beserta keluarganya melalui pemberian bantuan modal usaha.<br />7. Eks korban penyalahgunaan napza<br />Sesorang yang pernah menggunakan narkotika, psikotropika dan zat-zat adiktif lainnya termasuk minuman keras di luar tujuan pengobatan atau tanpa sepengetahuan dokter yang berwenang.<br />8. Anak, wanita dan lanjut usia korban tindak kekerasan<br />Anak berusia 5-18 tahun, wanita yang berusia 18-59 tahun dan lanjut usia yang berusia 60 tahun keatas yang terancam secara fisik atau non fisik (psikologis) yang mengalami tindak kekerasan, diperlakukan salah satu atau tidak semestinya dalam lingkungan keluarga atau lingkungan sosial terdekatnya.<br />9. Penyandang cacat<br />Pelayanan sosial yang diberikan bagi penyandang cacat adalah pemberian bantuan dana jaminan sosial bagi penyandang cacat berat melalui Departemen Sosial RI.<br />10. Eks Kusta<br />Eks kusta adalah sesorang yang pernah menderita penyakit kusta dan telah dinyatakan sembuh secara medis, tetapi mengalami hambatan untuk melaksanakan kegiatan sehari-hari karena dikucilkan keluarga atau masyarakat. Penanganan yang diberikan bagi eks kusta adalah pembedayaan keluarga eks kusta.<br />11. Eks Narapidana<br />Eks narapidana adalah seseorang yang telah selesai masa hukuman atau masa pidananya sesuai dengan keputusan pengadilan dan mengalami hambatan untuk menyesuaikan diri kembali dalam kehidupan masyarakat sehingga mendapat kesulitan untuk mendapatkan kehidupannya secara normal.<br />12. Lanjut Usia terlantar<br />Penanganan terhadap lanjut usia terlantar yang masih produktif yaitu pemberdayaan lanjut usia melalui pemberian bantuan usaha ekonomis produktif dan kelompok usaha bersama. Selain itu ada juga pemberian bantuan pelayanan dan jaminan sosial lanjut usia terlantar yang berasal dari Departemen Sosial RI.<br />13. Wanita Rawan Sosial Ekonomi<br />Wanita rawan sosial ekonomi adalah seorang wanita dewas berusia 18-59 tahun belum menikah atau janda dan tidak mempunyai penghasilan cukup untuk memenuhi kebutuhan pokok sehari-hari.<br />14. Keluarga Fakir Miskin<br />Pelayanan sosial yang diberikan bagi keluarga fakir miskin yaitu pengembangan potensi keluarga fakir miskin, pemberian latihan keterampilan berusaha bagi keluarga fakir miskin, pendampingan UEP dan KUBE fakir miskin.</p>', 'kewenangan-dinas-sosial.html', 'Y', 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
  `id_komentar` int(10) NOT NULL AUTO_INCREMENT,
  `id_artikel` int(10) NOT NULL,
  `nama_komentar` varchar(50) NOT NULL,
  `email_komentar` varchar(100) NOT NULL,
  `tanggal_komentar` int(50) NOT NULL,
  `isi_komentar` text NOT NULL,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id_komentar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `manajemen_halaman`
--

CREATE TABLE IF NOT EXISTS `manajemen_halaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `halaman` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `manajemen_halaman`
--

INSERT INTO `manajemen_halaman` (`id`, `halaman`) VALUES
(1, '[{"id":"24032295","children":[{"id":"2354885"},{"id":"364212"},{"id":"21042115"},{"id":"21042164"},{"id":"220410249"}]},{"id":"22044282","children":[{"id":"220411145"},{"id":"22047208"}]},{"id":"10411211"},{"id":"31031"}]');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nomor_penting`
--

CREATE TABLE IF NOT EXISTS `nomor_penting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL,
  `nomor` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data untuk tabel `nomor_penting`
--

INSERT INTO `nomor_penting` (`id`, `nama`, `nomor`) VALUES
(37, 'RSU Wahidin Sudirohusodo', '0411-584-677'),
(38, 'RSU Pelamonia', '0411-319-381'),
(39, 'RSU Labuang Baji', '0411-873-482'),
(40, 'RS Akademis Jaury', '0411-317-343'),
(41, 'RS Bhayangkara', '0411-836-344'),
(42, 'RS Bersalin Siti Khadijah IV', '0411-458992'),
(43, 'RS Bersalin Sentosa', '0411-3624248');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(150) NOT NULL,
  `nip` varchar(96) NOT NULL,
  `kelompok_bidang` int(11) NOT NULL,
  `pangkat` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `email` varchar(120) NOT NULL,
  `foto` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama_lengkap`, `nip`, `kelompok_bidang`, `pangkat`, `jabatan`, `tanggal_lahir`, `email`, `foto`) VALUES
(1, 'dedi indrayana', '', 1, 'kepala dinas', 'kepala dinas kesehatan rumah tangga', '2015-03-29', 'lopisquenak@gmail.com', '55170ee9cc3c9.png'),
(2, 'ririn rosalina', '', 2, '-', '-', '1979-03-08', 'lopisquenak@gmail.com', '551ac1de521ff.png'),
(3, 'lola tariningrum', '', 3, '-', '-', '1964-03-01', '', '551aceb46aefa.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE IF NOT EXISTS `pengaturan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(150) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `fb` varchar(75) NOT NULL,
  `tw` varchar(75) NOT NULL,
  `lon` varchar(120) NOT NULL,
  `lat` varchar(120) NOT NULL,
  `telp` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `logo`, `nama`, `alamat`, `fb`, `tw`, `lon`, `lat`, `telp`) VALUES
(1, 'logo.png', 'Dinas Sosial  Makassar', 'Jl.A.R Hakim No.50 Makassar', '359619264205339', 'dinsosmks', '119.40793266137689', '-5.133524486417152', '0411-584-677');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE IF NOT EXISTS `pengumuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `konten` text NOT NULL,
  `tanggal` date NOT NULL,
  `link` varchar(155) NOT NULL,
  `gambar` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `judul`, `konten`, `tanggal`, `link`, `gambar`) VALUES
(15, 'Pengumuman Penetapan Pemenang Perjalanan Dinas Luar Daerah ', '', '2015-03-25', 'Pengumuman-Penetapan-Pemenang-Perjalanan-Dinas-Luar-Daerah-.html', '5512e90ae9319.jpg'),
(16, 'Pengumuman Lelang', '', '2015-03-25', 'Pengumuman-Lelang.html', '5512e996d119e.jpg'),
(17, 'Pengumuman Pemenang Lelang', '', '2015-03-26', 'Pengumuman-Pemenang-Lelang.html', '5512ecc1e8bae.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `running_text`
--

CREATE TABLE IF NOT EXISTS `running_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konten` text NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `running_text`
--

INSERT INTO `running_text` (`id`, `konten`, `tanggal`) VALUES
(1, 'Selamat datang diportal Dinas Sosial Kota Makassar ... Anda dapat melihat program kerja Dinas&nbsp;Sosial, agenda Walikota serta Kepala Dinas, mengikuti pemberitaan serta melihat pengumuman resmi dari Dinas Kesehatan Makassar ....', '2015-04-22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur_organisasi`
--

CREATE TABLE IF NOT EXISTS `struktur_organisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kelompok` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `struktur_organisasi`
--

INSERT INTO `struktur_organisasi` (`id`, `kelompok`) VALUES
(1, 'Kepala Dinas'),
(2, 'sekretariat'),
(3, 'Bidang Pengkajian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_struktur_organisasi`
--

CREATE TABLE IF NOT EXISTS `sub_struktur_organisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_struktur` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `nama_sub` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `uraian`
--

CREATE TABLE IF NOT EXISTS `uraian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_halaman` int(11) NOT NULL,
  `uraian` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `uraian`
--

INSERT INTO `uraian` (`id`, `id_halaman`, `uraian`) VALUES
(9, 2, 'Meningkatnya status kesehatan dan gizi masyarakat.'),
(11, 2, 'Menurunnya angka kesakitan akibat penyakit menular.'),
(12, 2, 'Menurunnya disparitas status kesehatan dan status gizi antarwilayah dan antar tingkat sosial ekonomi serta Gender'),
(13, 2, 'Meningkatnya penyediaan anggaran publik untuk kesehatan dalam rangka mengurangi risiko finansial akibat gangguan kesehatan bagi seluruh penduduk, terutama penduduk miskin.'),
(14, 2, 'Meningkatnya PHBS pada tingkat rumah tangga dari 50% menjadi 70%'),
(15, 2, 'Terpenuhinya kebutuhan tenaga kesehatan strategis di DTPK.'),
(19, 27036289, 'Meningkatnya penyediaan anggaran publik untuk kesehatan dalam rangka mengurangi risiko finansial akibat gangguan kesehatan bagi seluruh penduduk, terutama penduduk miskin.'),
(20, 27036289, 'Peresmian Rumah Sakit Daerah'),
(21, 27032237, '123'),
(22, 27036289, '123'),
(23, 27036289, '123'),
(24, 27036289, '54331'),
(25, 27036289, 'percobaan'),
(26, 27039177, 'kepala dinas ini');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `nip` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`username`, `password`, `nama_lengkap`, `level`, `nip`, `id_session`, `email`, `blokir`) VALUES
('admin', '$2y$10$ae2xru0nX494I8pnCZlj2Og/eeBCP.eZeHjF/dOiUr582FTECokpK', 'admin', 'admin', '', 'lq2jsd5jm8uj6v0rfi94s9e827', 'lopisquenak@gmail.com', 'N'),
('dedi', '$2y$10$j0sO1k.5/leGcCftKIUeeuV4dKmWQtH5EcEu6Ylx28vZWJw5kwZ3a', 'dedi', 'umum', '11111', '8070rhmdf9780ghvuq8kbnjpu1', 'lopisquenak@gmail.com', 'N');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

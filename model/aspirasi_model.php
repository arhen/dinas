<?php

class aspirasi_model{
	private $db;
	public function __construct($database){
		$this->db = $database;
	}
	
	public function countAspirasi($id){
		$id = filter_var($id, FILTER_SANITIZE_STRING);
		$query = $this->db->prepare("select * from aspirasi ");
		
		
		$query->bindParam('id',$id);
		try{
			$query->execute();
			return $query->rowCount();
		}catch(PDOException $e){
			return false;
		}
			
	}
	
	public function countAspirasiById($id){
		$id = filter_var($id, FILTER_SANITIZE_STRING);
		$query = $this->db->prepare("select * from aspirasi where date(`tanggal`) = curdate() and id_session = :id");
		
		
		$query->bindParam('id',$id);
		try{
			$query->execute();
			return $query->rowCount();
		}catch(PDOException $e){
			return false;
		}
			
	}
	
	public function getAspirasiById($id){

		$query = $this->db->prepare("select * from aspirasi where id	= :id");
		$query->bindParam('id',$id,PDO::PARAM_INT);
		try{
			$query->execute();
			
		}catch(PDOException $e){ 
			
		}
			return $query->fetch(PDO::FETCH_ASSOC);
	}
		
	public function getAspirasi(){

		$query = $this->db->prepare("select * from aspirasi where blokir != 'Y' order by  id DESC ");
		$query->bindParam('id',$id,PDO::PARAM_INT);
		try{
			$query->execute();
			
		}catch(PDOException $e){ 
			
		}
			return $query->fetchAll(PDO::FETCH_ASSOC);
	}
		
	public function updateDukungan($id){

		$query = $this->db->prepare("update aspirasi set dukungan = dukungan + 1  where id = :id order by  id DESC ");
		$query->bindParam('id',$id,PDO::PARAM_INT);
		try{
			$query->execute();
			
		}catch(PDOException $e){ 
			
		}
			return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	
	public function insertAspirasi($id_session,$ip,$judul,$aspirasi,$nama,$email){
		
		$query = $this->db->prepare("INSERT INTO `aspirasi` SET `id_session`=:id_session,
																`ip`=:ip,
																`judul`=:judul,
																`aspirasi`=:aspirasi,
																`nama`=:nama,
																`email`=:email,
																`tanggal`=NOW()
																");
																	
		$query->bindParam(':id_session',$id_session,PDO::PARAM_STR);
		$query->bindParam(':ip',$ip,PDO::PARAM_STR);
		$query->bindParam(':judul',$judul,PDO::PARAM_STR);
		$query->bindParam(':aspirasi',$aspirasi,PDO::PARAM_STR);
		$query->bindParam(':nama',$nama,PDO::PARAM_STR);
		$query->bindParam(':email',$email,PDO::PARAM_STR);
		try{
			$query->execute();
			return true;
		}
		catch(PDOException $e){
		
			return false;
		}		
	}
	
	
	public function insertDukungan($id,$session){
		
		$query = $this->db->prepare("INSERT INTO `dukungan` SET `id_session`= :session,
																`id_aspirasi`= :id,
																`tanggal` = NOW()
																");
																	
		$query->bindParam(':session',$session,PDO::PARAM_STR);
		$query->bindParam(':id',$id,PDO::PARAM_INT);

		try{
			$query->execute();
			return true;
		}
		catch(PDOException $e){
		
			return false;
		}		
	}
	
	public function countDukunganBySession($random,$id){
				
		$query = $this->db->prepare("select * from dukungan where id_aspirasi = :random and id_session=:session ");
		
		$query->bindParam(':random',$random);
		$query->bindParam(':session',$id);
		try{
			$query->execute();
			return $query->rowCount();
		}catch(PDOException $e){
			return false;
		}
		
	
	}
	public function countDukungan($id){
		
		$id = filter_var($id, FILTER_SANITIZE_STRING);
		if(is_numeric($id)){
		$query = $this->db->prepare("select * from dukungan where id_aspirasi='?' ");
		
		
		$query->bindParam(1,$id);
		try{
			$query->execute();
			return $query->rowCount();
		}catch(PDOException $e){
			return false;
		}
		}
	
	}

}
?>
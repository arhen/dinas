<?php 
ob_start();
require_once("connect/database.php");
function autoload($class){

	$namafile = $class.'.php';

	include_once $namafile;

}

spl_autoload_register('autoload');

try{
	$libs 		= new libs_model();
	$aspirasi 	= new aspirasi_model($insert);
	
	$home 		= new home_model($select);
	$halaman 	= new halaman_model($select);
	$manajemen_halaman 	= new manajemen_halaman_model($select);
	$artikel	= new artikel_model($select);
	$event		= new event_model($select);
	$pengumuman	= new pengumuman_model($select);
	$galeri		= new galeri_model($select);
	$struktur_organisasi = new struktur_organisasi_model($select);
	$users	 	= new users_model($select);
	
}
catch(Exception $e){
	echo "Menemukan kesalahan : ".$e->getMessage()."\n";
}

?>
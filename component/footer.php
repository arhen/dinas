<?php if(isset($method)):?>
            <div class='container--action-bar'>

                <div class='action-bar'>

                    <div class='action-bar__title'>

                        <h2><a href='<?php echo ROOT."aspirasi.html";?>'>UNGKAPKAN ASPIRASI ANDA<i class='fa fa-arrow-circle-right'></i></a></h2>

                    </div>

                    <div class='action-bar__content'>

                        <p>Ungkapkan aspirasi anda baik dukungan, keluhan, ide, atau pengaduan anda dalam halaman <a href='<?php echo ROOT."aspirasi";?>'>Aspirasi Masyarakat</a>. Anda juga dapat menyebarkan dan mendukung aspirasi masyarakat lain.</p>

                    </div>

                </div>

            </div>







        	<div class='container--footer--widget'>

        		<div class='footer--widget'>

        			<div class='widget widget--address'>

    					<div class='widget__title'>
    						<h2>
    							ALAMAT
    						</h2>
    					</div>


    					<div class='widget__content'>

    						<div class='widget__content--address'>
    							<?php
								$pengaturan = $home->getPengaturan();
								echo "
								<p><strong>".$pengaturan['nama']."</strong></p>
    							<p>".$pengaturan['alamat']."</p>
    							<p>Telepon : ".$pengaturan['telp']."</p>
								";
								
								?>

    						</div>

    					</div>

    				</div>






    				<div class='widget widget--link'>

    					<div class='widget__title'>
    						<h2>
    							TAUTAN
    						</h2>
    					</div>


    					<div class='widget__content'>

    						<ul class='widget__content--link'>


							<?php 
							$footer_halaman = $halaman->getHalaman(0,14);
							echo "
    							<li>
    								<a href='".ROOT."'>Beranda</a>
    							</li>
    							<li>
    								<a href='".ROOT."kontak'>Kontak</a>
    							</li>							
							";
							
														
							foreach($data as $parrent)
									{ 
									
									$main = $halaman->getHalamanById($parrent['id']);
									
									if(isset($parrent['children']))
										{
										echo "	<li>
													<a href='".ROOT.'tentang/'.$main['id_halaman'].'/'.$main['link_halaman']."'>".$main['judul_halaman']."</a> 
												</li>
											";

											foreach($parrent['children'] as $sub)
											{
												
												$sub = $halaman->getHalamanById($sub['id']);
												
										echo "  <li> 
													<a href='".ROOT.'tentang/'.$sub['id_halaman'].'/'.$sub['link_halaman']."'>".$sub['judul_halaman']."</a> 
													
												</li>	"; 
											
											}//akhir sub 1
										
										}else{//parrent
								
								echo "  	<li>	
												<a href='".ROOT.'tentang/'.$main['id_halaman'].'/'.$main['link_halaman']."'>".$main['judul_halaman']."</a> 
											</li>
											"; 
										
										}//parrent
									
									}//
							?>
    						</ul>

    					</div>

    				</div>





    				<div class='widget widget--statistics'>

    					<div class='widget__title'>
    						<h2>
    							STATISTIK
    						</h2>
    					</div>


    					<div class='widget__content'>

    						<ul class='widget__content--statistics'>

    							<li>
    								Sedang Online <span class='statistics__count'>1</span>
    							</li>

    							<li>
    								Hari ini <span class='statistics__count'>49</span>
    							</li>

    							<li>
    								Minggu ini <span class='statistics__count'>171</span>
    							</li>

    							<li>
    								Keseluruhan ini <span class='statistics__count'>732</span>
    							</li>

    						</ul>

    					</div>

    				</div>







    				<div class='widget widget--contacts'>

    					<div class='widget__title'>
    						<h2>
    							NOMOR PENTING
    						</h2>
    					</div>


    					<div class='widget__content'>

    						<ul class='widget__content--contacts'>

							<?php
							
							$nomor_penting = $home->getNomorPenting();
							
							foreach($nomor_penting as $nomor_penting){
								
								echo"
										<li>
											".$nomor_penting['nama']." <span class='contacts__number'><br/><i class='fa fa-phone'></i>".$nomor_penting['nomor']."</span>
										</li>
										
									";
								
							}
							
							?>
							

    						</ul>

    					</div>

    				</div>

        		</div>

        	</div>





        	<div class='container--footer--copyright'>

        		<div class='footer--copyright'>
					<?php 
					
					echo "<p>&copy; Copyright ".date("Y").". Pemerintah Kota Makassar.</p>";
					
					?>
					
        		</div>

        	</div>
			
<?php endif;?>
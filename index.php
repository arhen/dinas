<?php 
@session_start();
require"model/class.php";
include("cms/libs/path.php");
$url = isset($_GET['p']) ? $_GET['p'] : null;
$url = rtrim($url, '/');
$url = filter_var($url, FILTER_SANITIZE_URL);
$url = explode('/', $url);      // memecah URL menjadi variabel dimana var pertama adalah :
#config dasar
$model 		= $url[0];
$method 	= !empty($url[1])?$url[1]:'';
$parameter 	= !empty($url[2])?$url[2]:null;
// $parameter = filter_var($parameter,FILTER_VALIDATE_INT);
// $method = filter_var(strip_tags($parameter),FILTER_VALIDATE_INT);
// $aa = $libs->timer();
if(empty($_SESSION['code'])){
	$libs->recode();
	 $_SESSION['aspirasi'] = uniqid();

	// hitung pengunjung 
		
}else{
	
	if(!$libs->cek_waktu()){

		// hitung lagi dan recode
		 $_SESSION['aspirasi'] = uniqid();
	}	
}
// var_dump($_SESSION['aspirasi']);
?>

<html>
	<head>
        <title>
            Dinas Sosial Kota Makassar
        </title>


        <!-- Mobile Specific Meta -->
		<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>

		<!-- Stylesheets -->
		<link rel='stylesheet' href='<?php echo ROOT;?>css/style.css' />

		<link rel='stylesheet' href='<?php echo ROOT;?>fonts/font-awesome-4.2.0/css/font-awesome.min.css'>

		
		<script src='<?php echo ROOT;?>scripts/min/jquery-min.js' type='text/javascript'></script>
		<script src='<?php echo ROOT;?>scripts/min/retina-min.js' type='text/javascript'/></script>
		<script src='<?php echo ROOT;?>scripts/min/slider-min.js' type='text/javascript'/></script>
		<script src='<?php echo ROOT;?>scripts/min/jquery.marquee-min.js' type='text/javascript'/></script>
		<script src='<?php echo ROOT;?>scripts/min/jquery.orgchart-min.js' type='text/javascript'/></script>
		<script src='<?php echo ROOT;?>scripts/min/jquery.lightSlider-min.js' type='text/javascript'/></script>
		<script src='<?php echo ROOT;?>scripts/min/readmore-min.js' type='text/javascript'/></script>
		<script src='<?php echo ROOT;?>scripts/min/custom-min.js' type='text/javascript'/></script>
		<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>


		<!--[if lt IE 9]>
			<script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
		<![endif]-->

    </head>





    <body>
        <div class='wrapper'>

        	<div class='container--top'>

        	    <div class='container--top__main'>

        	    	<div class='logo'>

        	    	    <div class='logo__image'>

        	    	        <img src='<?php echo ROOT;?>images/layout/logo.png' title='Kota Makassar'/>

        	    		</div>





        	    		<div class='logo__text'>

        	    		    <h2>Dinas Sosial <span class='logo__text__city'>Makassar</span></h2>

        	    		</div>

        	    	</div>





        	        <div class='search'>

        	    	    <div class='search__container'>

        	    		    <div class='search__container__icon'>

        	    			    <i class='fa fa-search'></i>

        	    			</div>

        	    			



        	    			<div class='search__container__form'>

        	    			    <input type='text' name='form__input-text' placeholder='PENCARIAN'/>

        	    			</div>

        	    		</div>

        	    	</div>

        	    </div>


        	    <?php 
				
					include "component/navigasi.php"; 
				
				?>
        	</div>

			
				<?php
					switch($model){ // pilih model
						default:
							include "controller/home/home.php";
						break;
						case "tentang":
							include "controller/tentang/tentang.php";
						break;
						case "berita":
							include "controller/berita/berita.php";
						break;
						case "agenda":
							include "controller/agenda/agenda.php";
						break;
						case "download":
							include "component/download.php"; //bukan konten, cuman untuk download
						break;
						case "kontak.html":
							include "controller/kontak/kontak.php"; //bukan konten, cuman untuk download
						break;
						case "arsip":
							include "controller/arsip/arsip.php"; //bukan konten, cuman untuk download
						break;
						case "pengumuman":
							include "controller/pengumuman/pengumuman.php"; //bukan konten, cuman untuk download
						break;
						case "galeri":
							include "controller/galeri/galeri.php"; //bukan konten, cuman untuk download
						break;
						case "galery":
							include "controller/galeri/galery.php"; //bukan konten, cuman untuk download
						break;
						case "struktur-organisasi.html":
							include "controller/struktur/struktur.php"; //bukan konten, cuman untuk download
						break;
						case "aspirasi.html":
							include "controller/aspirasi/aspirasi.php"; //bukan konten, cuman untuk download
						break;
					}

				?>


				<?php
				
					include "component/footer.php";
				
				?>


             <div class='body-background'><img src='<?php echo ROOT;?>images/layout/background.jpg'/></div>

        </div>

    </body>

</html>
 



<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

</script>

<script>window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));</script>
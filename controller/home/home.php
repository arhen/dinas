<div class='container--slider'>

        		<div class='slider'>

                    <div class='container--slider__slide'>
                        <ul id='slide'>
						<?php 
						$gal = $galeri->getGambarUtama('GAMBAR UTAMA',5);
						foreach($gal as $gambar_utama){
						echo "
						  <li>
                            <a href='javascript:;'><img style='width:100%; height:100%;' src='".ROOT."images/content/gallery/".$gambar_utama['nama_file']."' <!--alt='".$gambar_utama['keterangan']."'>--></a>
                          </li>
						";
						}
						?>
                         
                        </ul>
            		</div>




            		<div class='container--slider__agenda'>

            			<div class='agenda'>

            				<div class='agenda__title'>
            					
            					<h3>
            						<i class='fa fa-calendar-o widget__icon'></i><a href='#'>AGENDA TERDEKAT<i class='fa fa-arrow-circle-right'></i></a>
            					</h3>

            				</div>

            				<div class='agenda__content'>

            					<div class='agenda__content__section'>
									<!--
            						<div class='agenda__content__section__title'>
            							<h4>DINAS</h4>
            						</div>-->
            						
            						<div class='agenda__content__section__list'>

            							<ul class='agenda__list'>
										
										<?php
										$agenda = $event->getEvent(0,10);
										foreach($agenda as $agenda)
										{
											
											echo "
										
											<li>
												<a href='".ROOT."agenda/".$agenda['id']."/".$agenda['link']."'><i class='fa fa-map-marker'></i></a><p><a href='".ROOT."agenda/".$agenda['id']."/".$agenda['link']."'>".$agenda['judul']."</a></p><span class='agenda__list__date'>".date("d/m",strtotime($agenda['tanggal_mulai']))."</span>
											</li>
											";
										}
										?>
											
            								
											
            								
            							</ul>

            						</div>

            					</div>

            				</div>

            			</div>

            		</div>

                </div>

        	</div>





        	<div class='container--running-text'>
        		
        		<div class='running-text'>
        			
        			<div class='running-text__main'>
                        
                        <div class='running-text__main__content'>
                            <p>
							
							<?php
								
								echo $home->getRunningById()['konten'];
								// echo $home->insertNomor(1, 1);

							?>
							</p>
                        </div>

                    </div>

        		</div>
        	
        	</div>





        	<div class='container--content'>

        		<div class='content'>

        			<div class='content--main'>

        				<div class='main--section'>

        					<div class='main--section__title'>

        						<h3>
        						<?php 
								echo"
									<a href='".ROOT.'arsip'."'>BERITA<i class='fa fa-arrow-circle-right'></i></a>
        						";
								
								?>	
								
								</h3>

        					</div>

        					<div class='main--section__content'>

        						<div class='news-list'>

        							<ul>

									<?php
									$berita = $artikel->getArtikel(0,3);
									
									foreach($berita as $berita){
									echo"
										<li>

        									<div class='news-list__thumbnail'>";
												
										if(!empty($berita['gambar'])){	
										echo"	<img background-size:contain; style='width:270px;' src='".ROOT."images/content/news/thumbnails/".$berita['gambar']."'/>";
										}else{
											
										echo"	<img style='height:175;' src='".ROOT."images/content/logo.png'/>";
											
										}
										
										echo"      									
											</div>

        									<div class='news-list__meta'>

        										<p class='news-list__meta__date'>
        											".$libs->tgl_indo($berita['tanggal'])."
        										</p>

        									</div>

        									<div class='news-list__title'>

        										<h3><a href='".ROOT.'berita/'.$berita['id'].'/'.$berita['link']."'>".$berita['judul']."</a></h3>

        									</div>

        								</li>									
									
									";
									}
									?>
									

        							</ul>

        						</div>

        					</div>

        				</div>

        			</div>





        			<div class='content--sidebar'>

        				<div class='content--sidebar__widget'>

        					<div class='widget widget--announcement'>

        						<div class='widget__title'>
        							<h3>
        								<i class='fa fa-bullhorn widget__icon'></i><a href='#'>PENGUMUMAN</a>
        							</h3>
        						</div>


        						<div class='widget__content'>

        							<ul class='widget__content--announcement'>
										<?php 
										$pengumuman = $pengumuman->getPengumuman(0,5);
										foreach($pengumuman as $pengumuman){
										echo "
        								<li>
        									<p class='widget__content--announcement__date'>".$libs->tgl_indo($pengumuman['tanggal'])."</p>
        									<p class='widget__content--announcement__title'><i class='fa fa-flag'></i><a href='".ROOT."pengumuman/".$pengumuman['id']."/".$pengumuman['link']."'>".$pengumuman['judul']."</a></p>
        								</li>
											
										";
										}
										
										?>

        							</ul>

        						</div>

        					</div>

        				</div>

        			</div>

        		</div>

        	</div>





        	
        	<div class='container--content--fullwidth'>

        		<div class='content'>
					
					<div class='content--main'>

        				<div class='main--section'>

        					<div class='main--section__title--page'>

        						<h3>
                                 <?php echo "<a href='".ROOT."galery"."'>Galeri  &nbsp <i class='fa fa-arrow-circle-right'></i> </a>";?>   
                                </h3>

        					</div>

        					<div class='main--section__content'>

        						<div class='gallery-folder'>

                                    <ul>
										
										<?php 

										$gambar = $galeri->getGaleri(0,3);
										
										foreach($gambar as $gambar){
											
										$detail = $galeri->getGaleriDetilByIdGaleri($gambar['id']);
									
											
											$detail = $detail['nama_file'];
										
										echo "
										
										<li>

                                            <div class='gallery-folder__thumbnail'>";
											
										if(empty($detail)){
											
										echo " <a href='".ROOT."galeri/".$gambar['id']."' ><img style='height:230px; width:180px;' src='".ROOT."images/content/logo.png'/>";											
										}

										echo " <a href='".ROOT."galeri/".$gambar['id']."'><img src='".ROOT."images/content/gallery/thumbnails/".$detail."'/>";
                                                
												echo "
												<div class='gallery-folder__thumbnail__title'>

                                                    

                                                </div></a>

                                            </div>
											
											<div class='programs-list__title'>

        										<a href='".ROOT."galeri/".$gambar['id']."'><h3> ".$gambar['nama']."</a></h3>

        									</div>
											
											
                                        </li>
										
										
										";
										}
										?>
									
                                        
                                    </ul>

                                </div>

        					</div>

        				</div>

        			</div>
        			<div class='content--fullwidth'>

        				

        			</div>

        		</div>

        	</div>






        	
        	<div class='container--content--cards'>

        		<div class='cards'>

        			<div class='cards--badge'>

        				<div class='main--section'>

        					<div class='main--section__title'>

        						<h3>
        							<a href='#'>CITY BADGE<i class='fa fa-arrow-circle-right'></i></a>
        						</h3>

        					</div>

        					<div class='main--section__content'>

        						<ul class='badge__thumbnail'>
        							<li>
                                        <a href='#'><img src='<?php echo ROOT;?>images/content/badges/1.jpg'/></a>
                                    </li>
                                    <li>
                                        <a href='#'><img src='<?php echo ROOT;?>images/content/badges/2.jpg'/></a>
                                    </li>
                                    <li>
                                        <a href='#'><img src='<?php echo ROOT;?>images/content/badges/3.jpeg'/></a>
                                    </li>
                                    <li>
                                        <a href='#'><img src='<?php echo ROOT;?>images/content/badges/2.jpg'/></a>
                                    </li>
        						</ul>

        					</div>

        				</div>

        			</div>






        			<div class='cards--twitter'>

        				<div class='main--section'>

        					<div class='main--section__title'>

        						<h3>
        							<a href='#'>TWITTER<i class='fa fa-arrow-circle-right'></i></a>
        						</h3>

        					</div>

        					<div class='main--section__content'>

        						<div class='twitter__content'>
        								<a class="  button--square button--white twitter-follow-button"
								  href="https://twitter.com/<?php echo trim($home->getPengaturan()['tw']);?>"
								  data-show-count="true"
								  data-lang="en"
								  data-width="300px"
								  data-align="left" >
								
								</a>
								<br/>
									

							<div id="fb-root"></div>

								<div class="fb-like-box" data-href="https://www.facebook.com/<?php echo trim($home->getPengaturan()['fb']);?>" data-header="false"  data-width="330" data-connections="5" data-height="212"  data-colorscheme="dark" data-show-faces="true" data-stream="false" data-show-border="false"></div>
        						</div>

        					</div>

        				</div>

        			</div>





        			<div class='cards--content--cards' >

        				<div class='main--section'>
						
						<center>
        					
							<div class='main--section__title'>

        						<h3>
        							<a href='#'>Kepala Dinas Sosial Kota Makassar</a>
        						</h3>

        					</div>

        					<div class='main--section__content'>

        						<div class='facebook__content'>
        							Drs. H. Yunus Said, M.Si.<p><img src='<?=ROOT?>images/content/organization/kadis.jpg' style='height:190px;'></p>

        							<!--<button class='button--square button--white' href='#'>FOLLOW</button>-->
        						
								</div>

        					</div>

						</center>
						
        				</div>

        			</div>

        		</div>

        	</div>
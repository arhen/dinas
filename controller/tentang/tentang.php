<div class='container--content--page'>

	<div class='content'>

		<div class='content--main'>

			<div class='main--section'>

				
				<?php 
				$method  = filter_var($method,FILTER_VALIDATE_INT);
				if(!is_numeric($method)){
					header('location:'.ROOT.'');
				}
				
				$detail = $halaman->getHalamanById($method);
				
					if(!empty($detail['gambar_halaman'])){
						
					echo"<div class='main--section__featured-image'>"; 	
							
						
						echo "<img  src='".ROOT."upload/".$detail['gambar_halaman']."'/>"; //dummy
					
					
					echo"</div>";
					
					}{
						echo "<div id='picture' height='400px'></div>";
						
					};
						// echo "<img src='".ROOT."images/content/featured-images/selayang-pandang.jpg'/>";
				
				?>
				
				

				<div class='main--section__title'>

					<h1>
						<?php echo $detail['judul_halaman']?>
					</h1>

				</div>

				<div class='main--section__content'>

					<div class='main--content--text'>
					   <?php echo $detail['konten_halaman'];
					   
					   ?>

					</div>
					<?php
						$uraian = $halaman->getUraian($detail['id_halaman']);
						if(!empty($uraian)){
							$no=1;
					echo "<ul class='mission-list'>";
							foreach($uraian as $uraian){
					  
					  echo "<li>
								<span class='mission-list__number'>".$no."</span>
								<p>".$uraian['uraian']."</p>
							</li>";
								
								$no++;
								
							}
							
					echo "</ul>";	
						}

						$file = $halaman->getFile($detail['id_halaman']);

						if(!empty($file)){
							
							echo "<ul class='download-list'>
										<li class='download-list__header'>
											Berkas
										</li>";
									foreach($file as $file){
							echo "		<li>
											<a href='".ROOT."download/".$file['nama_file']."'><i class='fa fa-file-pdf-o widget__icon'></i> ".$file['judul']."</a>
										</li>";
									}
										
							echo "</ul>
									
								 ";
							
							
						}
					
					?>

				</div>
				
				
				

			</div>

		</div>


		<?php include "component/sidebar.php";?>

	</div>






	<div class='clear'></div>

</div>
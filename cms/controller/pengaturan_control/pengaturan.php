<?php 
date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/pengaturan_control/pengaturan_control.php?model=pengaturan&method="; // halaman untuk eksekusi

echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget purple'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Pengaturan</h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
";
	
	 $detail = $pengaturan->getPengaturanById();

	echo "
<div class='widget-body'>											
	<h3> Pengaturan </h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal'>
			<div class='control-group'>
				<label class='control-label'>Nama Website</label>
				<div class='controls'>
					<input type='text' class='input-medium' name='nama' placeholder='nama' value='".$detail['nama']."' required>
					<span class='help-inline'></span>
				</div>
			</div>
			
			<div class='control-group'>
				<label class='control-label'>Alamat</label>
				<div class='controls'>
					<input type='text' class='input-xxlarge' name='alamat' id='alamat' placeholder='Alamat' value='".$detail['alamat']."' required>
					<span class='help-inline'></span>
				</div>
			</div>
			
			<div class='control-group'>
				<label class='control-label'>Facebook</label>
				<div class='controls'>
					<input type='text' class='input-medium' name='fb' id='fb' placeholder='Alamat Facebook' value='".$detail['fb']."' required>
					<span class='help-inline'></span>
				</div>
			</div>
			
			<div class='control-group'>
				<label class='control-label'>Twitter</label>
				<div class='controls'>
					<input type='text' class='input-medium' name='tw' id='tw' placeholder='Alamat Twitter' value='".$detail['tw']."' required>
					<span class='help-inline'></span>
				</div>
			</div>
			
			<div class='control-group'>
				<label class='control-label' >Telepon</label>
				<div class='controls'>
					<input type='text' class='input-medium' name='telp' placeholder='Telepon' value='".$detail['telp']."' required>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->

			<div class='control-group'>											
				<label class='control-label' >Location</label>
				<div class='controls'>
					<b>pindahkan icon warna merah ke lokasi kantor anda </b><br/>
					<br/>
					<fieldset class='gllpLatlonPicker'>
						<div class='gllpMap'>Google Maps</div>
						Latitude : <input type='text' name='lat' class='gllpLatitude' value='-5.133439'/>
						Longitude : <input type='text' name='lon' class='gllpLongitude' value='119.407761'/>
						<input type='hidden' name='zoom' class='gllpZoom' value='16'/>
					</fieldset>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
		
			<div class='control-group'>											
				<label class='control-label' > </label>
				<div class='controls'>
					<button type='submit' name='tambah' value='tambah' class='btn btn-primary'>Submit</button>
					 
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->

		</form>	";
echo "
					
</div>	
				
</div>	
		
	";
?>
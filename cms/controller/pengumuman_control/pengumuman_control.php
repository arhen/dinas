 <?php 
// session_start();
if($_REQUEST['model']): // for secure single file
ob_start();
date_default_timezone_set('Asia/Jakarta');

require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];


// echo $method;
// echo $parameter;

if($model=='pengumuman' AND $method=='tambah' ){
	
 	if(isset($_POST['tambah'])){
		$judul = $_POST['judul'];
		$judul = filter_var($judul, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 

 		$link = $libs->changeLink($judul);
		
		$isi = $_POST['konten'];
		
		$tanggal = date("Y-m-d", strtotime($_POST['tanggal']));
		
		// $isi = $libs->stringHtml($isi);
		
		if(isset($_FILES['file']['name'])){

			// $gambar = $libs->uploadImage($_FILES['file']);	// ke folder upload
			$gambar = $libs->uploadImageToFolder('../../../upload/',$_FILES['file']);	//upload
			
		}

		$pengumuman->insertPengumuman($judul,$link,$isi,$tanggal,$gambar); // method penyimpanan

		echo"<script> alert('Menambah data'); </script>";
		
	}
 
 // return false;
 }

if ($model=='pengumuman' AND $method=='edit' ){
	if(isset($_POST['edit'])){
		$judul = $_POST['judul'];
	
		$judul = filter_var($judul, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 

		$id = $_POST['id'];
		
		$isi = $_POST['konten'];
		
		$gambar = $_POST['gambar'];
		
	 	$tanggal = date("Y-m-d", strtotime($_POST['tanggal']));
		
		$isi = filter_var($isi, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		// $isi = $libs->stringHtml($isi);
		
		$link = $libs->changeLink($judul);
				
				
		if(!empty($_FILES['file']['name'])){
			
			// $libs->hapusFile($gambar);
					
			$libs-> hapusGambarSpesific("../../../upload/",$gambar);

			$gambar = $libs->uploadImageToFolder('../../../upload/',$_FILES['file']);	//upload
		
		}
		$pengumuman->updatePengumuman($judul,$link,$isi,$tanggal, $id,$gambar); // method penyimpanan
	}
}	
if($model=='pengumuman' AND $method=='hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $pengumuman->getPengumumanById($id);
	$libs-> hapusGambarSpesific("../../../upload/", $data['gambar']);
	$pengumuman->deletePengumuman($id);

 echo"<script> alert('Menghapus data'); </script>";
}
 header("location:".URL."pengumuman");
 
 endif;
?>
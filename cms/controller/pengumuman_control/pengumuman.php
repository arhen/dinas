<?php 
if(isset($method)):

date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/pengumuman_control/pengumuman_control.php?model=pengumuman&method="; // halaman untuk eksekusi

echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget yellow'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Pengumuman </h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>
";
// var_dump($method);
switch($method){

default :

	echo "
	<a href='".URL."pengumuman/tambah' class='btn btn-success'>Tambah Pengumuman</a>
	<br/>
	<br/>
	<table id='example2' class='table table-bordered table-striped ' >
	<thead>
		<tr >
			<th>No</th>
			<th style='width:30%;'>Judul</th>
			<th style='width:40%;'>Konten</th>
			<th>Tanggal</th>
			<th>Tindakan</th>
		</tr>
		
	</thead>
	<tbody>
	" ; // header tabel

	$semua =  $pengumuman->countPengumuman();
	
		$per_page = 17; // jumlah query per pengumuman 

		$pages = ceil($semua / $per_page); // melihat total blok yang ada
		
		$page = (isset($_GET['hal']))?(int)$_GET['hal'] :1; // default page
		
		$start = ($page-1)*$per_page; //startnya 
		
		$no = ($page-1)*$per_page +1; // menentukan asending nomor tiap pengumuman paging
		
		$pengumuman = $pengumuman->getPengumuman($start,$per_page); //menghitung 
		
		if($pages==0){echo "Data tidak ditemukan"; }


	foreach($pengumuman as $pengumuman){	
		
		echo "
		<tr>
			<td>".$no."</td>

			<td>".stripslashes(htmlspecialchars($pengumuman['judul']))."</td>

			<td>".substr(strip_tags($pengumuman['konten']),0,96)."</td>
			
			<td>".date("d-m-Y",strtotime($pengumuman['tanggal']))."</td>

		
			<td>
			
				<div class='btn-group'>
					<button data-toggle='dropdown' class='btn btn-small btn-success dropdown-toggle'>Tindakan <span class='caret'></span></button>
					 <ul class='dropdown-menu'>
						 <li><a href='".URL."pengumuman/edit/".$pengumuman['id']."'><i class=' icon-edit'></i>Edit </a></li>
						 <li><a href=\"javascript: hapusAlert('".$pengumuman['id']."');\"><i class=' icon-trash'></i>Hapus</a></li>
					 </ul>
				</div>
		
			</td>
	
		</tr>
		
		";
		$no++;
	}
	
	echo "
	</tbody>	
	</table>

	";echo "<center> <div class='pagination'> <ul class='pagination pagination-small center m-t-none m-b-none'>";

			// $req = empty($_REQUEST['view'])?'':$_REQUEST['view']; // mencari kata carinya
			
			$root = URL.'pengumuman';//$_SERVER['PHP_SELF']; //."?view=".$req."&"; 
			
			$blok = 10;
			$ini  = ceil($page/$blok);
			
			$mulai   =  ($blok * $ini) - ($blok-1);
			$selesai =  ($blok<=$pages)?($ini * $blok):$pages ;
			
			$kurang1 = $page -1;
			$tambah1 = $page +1 ;
			
			if($pages >=1 && $page<=$pages){

				echo ($page!=1 or empty($page))?"<li style='cursor:pointer;'  ><a href='$root&hal=$kurang1'> Back </a> </li> ":'';
				for($x=$mulai; $x<=$selesai; $x++)
					if($x==$page){
						
						echo "<li style='cursor:pointer;' class='active'> <a >$x</a> </li> ";
						
					}
					else{ 
						echo "<li style='cursor:pointer;'  ><a href='$root?hal=$x'>$x</a></li> ";
					};
				
				echo ($page!=$pages)?"<li style='cursor:pointer;'  ><a href='$root?hal=$tambah1'> Next </a> </li> ":'';
									
				
			};
	echo "</ul> </div></center>";

	break;

case "tambah":
	echo "
	<h3> Tambah Pengumuman </h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."tambah' method='POST' name='form' class='form-horizontal'>
		
			<div class='control-group'>
				<label class='control-label'>Judul</label>
				<div class='controls'>
					<input type='judul' class='form-control' name='judul' id='judul' placeholder='judul' required>
				</div>
			</div>
			<div class='control-group'>
				<label class='control-label'>Konten</label>
				<div class='controls'>
					<textarea name='konten' class='form-control'> </textarea>
				</div>
			</div>
			
			<div class='control-group'>
				<label class='control-label'>Gambar (MAX 1Mb)</label>
				<div class='controls'>
					<input type='file' name='file' id='uploadFile' accept='image/*' />
				</div>
			</div>
			<div class='control-group'>
				<label class='control-label'>Tanggal</label>
				<div class='controls'>
					<input type='text' name='tanggal' readonly value='".date("d-m-Y")."'/>
				</div>
			</div>
			
			<label class='control-label'> </label>
			<div class='controls'>
				<button type='submit' name='tambah' value='edit' class='btn btn-primary'>Submit</button>
			</div>

		</form>
	
	<div id='imagePreview' style='width:400px; height:266px;'></div>
	
	";

break;
	
case "edit":
	if(filter_var($parameter, FILTER_VALIDATE_INT)){
		
	 $pengumuman = $pengumuman->getPengumumanById($parameter);
	
	echo "
	<h3> Edit Pengumuman </h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal'>
				<div class='control-group'>
					<label class='control-label'>Judul</label>
					<div class='controls'>
						<input type='text' class='form-control' name='judul' id='judul' placeholder='judul' value='".$pengumuman['judul']."' required>
						<input type='hidden' class='form-control' name='id' value='".$pengumuman['id']."' >
						<input type='hidden' class='form-control' name='gambar' value='".$pengumuman['gambar']."' >
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Isi</label>
					<div class='controls'>
						<textarea  name='konten' class='form-control'>
		
							".$pengumuman['konten']."
						
						</textarea>
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Tanggal</label>
					<div class='controls'>
						<input type='text' name='tanggal' readonly value='".$pengumuman['tanggal']."'/>
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Gambar (MAX 1Mb)</label>
					<div class='controls'>
						<input type='file' name='file' id='uploadFile' accept='image/*' />
					</div>
				</div>
				
				<label class='control-label'></label>
					<div class='controls'>
						<button type='submit' name='edit' value='edit' class='btn btn-primary'>Submit</button>
					</div>
		
		</form>
<img src='".ROOT.'upload/'.$pengumuman['gambar']."' style='width:400px; height:266px;' />
<div id='imagePreview' style='width:400px; height:266px;'></div>
	";

break;

}else{
		header("location:".URL."pengumuman");
		return false;
		exit();
	}
}

?>
<script type='text/javascript'> 
 function hapusAlert(iddokumen){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ 
			location.href="<?php echo $aksi."hapus" ;?>&id="+ iddokumen;
		}else{
			return false;
		}
	};

</script>

<?php 
echo " 		</div>
		</div>
    </div>";//end of wrapper
endif;?>
<?php 
if(isset($method)):
echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget orange'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Struktur Organisasi</h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>";


date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/struktur_organisasi_control/struktur_organisasi_control.php?model=struktur_organisasi&method="; // galeri untuk eksekusi


// var_dump($method);
switch($method){

default :
$acak = date('dm').mt_rand(1,17).mt_rand(99,300);
	echo "
	<a href='".URL."struktur_organisasi/tambah/".$acak."' class='btn btn-primary'>Tambah Bagian / Divisi / Seksi</a>
	<br/>
	<br/>
	<table id='example2' class='table table-bordered table-striped ' >
	<thead>
		<tr >
			<th>No</th>
			<th>Nama Bidang / Bagian /Seksi /Fungsi </th>
			<th>Tindakan</th>
		</tr>
		
	</thead>
	<tbody>
	" ; // header tabel

	$no= 1;
			
	$so = $struktur_organisasi->getStrukturOrganisasi(0,999); //menghitung 
				
	foreach($so as $so){	
		$nama = $so['kelompok'];
		echo "
		<tr>
		
			<td>".$no."</td>


			<td><a href='".URL."struktur_organisasi/detail/".$so['id']."'>".$so['kelompok']."</a></td>

		<td> <a href='".URL."galeri/edit/".$so['id']."' class='btn btn-small btn-success'>Ubah Nama </a> | <a href=\"javascript: hapusAlert('".$so['id']."');\" class='btn btn-small btn-danger'>Hapus </a>
	
		</tr>
		
		";
		$no++;
	}
	echo "
	</tbody>	
	</table>
	";
	
	break;

case "tambah":

 $id= $parameter;

	echo "
		<form role='form' enctype='multipart/form-data' action='".$aksi."tambah' method='POST' name='form' class='form-horizontal'>
			<div class='control-group'>											
				<label class='control-label' >Nama Bagian / Divisi / Seksi / Fungsi</label>
				<div class='controls'>
					<input type='text' class='form-control' name='nama' id='nama' placeholder='nama' required />
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
			
			<div class='control-group'>											
				<label class='control-label' > </label>
				<div class='controls'>
					<button type='submit' name='submit' value='Tambah' class='btn btn-primary'>Submit</button>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->	
					
		</form>";

break;

case "detail":

	if(filter_var($parameter, FILTER_VALIDATE_INT)){
	$pegawai  = $struktur_organisasi->getStrukturOrganisasiById($parameter);
	
	echo "<h2>Nama Bidang / Seksi /Fungsi  : ".$pegawai['kelompok']."</h2>";		
			
	echo "<form role='form' class='form-horizontal' enctype='multipart/form-data' action='".$aksi."pegawai_tambah' method='POST' name='form' class='form-horizontal'>	
		<div class='control-group'>										
				<label class='control-label' >Nama Lengkap </label>
			<div class='controls'>
				<input type='text' name='nama' placeholder='Nama Lengkap' /> <br/>
				<input type='hidden' name='kelompok_bidang' value='".$parameter."'/> 
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		<div class='control-group'>										
				<label class='control-label' >Pangkat </label>
			<div class='controls'>
				<input type='text' name='pangkat' placeholder='Pangkat' /> <br/>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		<div class='control-group'>										
				<label class='control-label' >Jabatan </label>
			<div class='controls'>
				<input type='text' name='jabatan' placeholder='Jabatan' /> <br/>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		<div class='control-group'>										
				<label class='control-label' >Tanggal Lahir </label>
			<div class='controls'>
				<input type='text' id='dp1' name='tanggal' value='".date('d-m-Y')."' /> <br/>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->		
		<div class='control-group'>										
				<label class='control-label' >Email </label>
			<div class='controls'>
				<input type='text' name='email' placeholder='email'/> <br/>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->							
				
		<div class='control-group'>											
			<label class='control-label' >Foto</label>
			<div class='controls'>
				<b>*Format Gambar hanya png dan jpg<br/></b>
				<input type='file' name='file' />  <br/>	
				<input type='hidden' class='form-control' value='".$parameter."' name='id' />
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
			
		
		<div class='control-group'>											
			
			<div class='controls'>
				<input type='submit' name='pegawai' value='Tambahkan'  class='btn btn-success'>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
		
	</form><br/> ";
	
	echo "<table class='table'>
			<tr>
				<th>No</th>
				<th>Nama Lengkap</th>
				<th>Pangkat</th>
				<th>Jabatan</th>
				<th>Tanggal Lahir</th>
				<th>Email</th>
				<th>Foto</th>
			</tr>
	
		";
	$pegawai = $struktur_organisasi->getPegawai($parameter);
	// var_dump($pegawai);
	$no = 1;
	foreach($pegawai as $pegawai){
	echo "<tr> 
			<td>".$no." </td>
			<td>";
			
			echo $pegawai["nama_lengkap"];
	
	echo "	</td>
			
			<td>
				".$pegawai['pangkat']."
			</td>
	
			<td>
				".$pegawai['jabatan']."
			</td>
	
			<td>
				".$pegawai['tanggal_lahir']."
			</td>
	
			<td>
				".$pegawai['email']."
			</td>
	
			<td>
				<img style='width:40px; height:45px;' src='".ROOT."images/content/organization/".$pegawai['foto']."'>
			</td>
	
			<td>
				<a href='".$aksi."pegawai_hapus&id=".$pegawai['id'] ."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a>
			</td>
		 </tr>";
	$no++;
	}
	
	echo "</table>";

	}
break ;
case "edit":
	// var_dump($parameter);
	if(filter_var($parameter, FILTER_VALIDATE_INT)){
	
	$detail = $galeri->getGaleriById($parameter);

		
	echo "
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal'>
			<div class='control-group'>											
				<label class='control-label' >Nama Album</label>
				<div class='controls'>
					<input type='text' class='form-control' name='nama' id='nama' placeholder='nama' required value='".$detail['nama']."' />
					<input type='hidden' class='form-control' name='id' value='".$detail['id']."' />
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
			
			<div class='control-group'>											
				<label class='control-label' > </label>
				<div class='controls'>
					<button type='submit' name='submit' value='Ubah' class='btn btn-primary'>Submit</button>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->	
					
		</form>";

break;

}else{
		header("location:".URL."galeri");
		return false;
	}
}

?>
<script type='text/javascript'> 
 function hapusAlert(iddokumen){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ //modul/mod_berita/beritaAksi.php?act=berita&page=hapus&id=$berita[id]
			location.href="<?php echo $aksi."hapus" ;?>&id="+ iddokumen;
		}else{
			return false;
		}
	};

</script>

<?php 
echo "
		</div>
	</div>
</div>
";

endif;?>
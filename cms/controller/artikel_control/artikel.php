<?php 
if(isset($method)):

date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/artikel_control/artikel_control.php?model=berita&method="; // halaman untuk eksekusi
echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget red'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Berita </h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>
";


// var_dump($method);
switch($method){

default :
	echo "
		<form  method='get' action=''>
			<div style='float:left; '>
				<input id='cari_stok' class='form-control col-md-4' style='width:250px;' type='text' name='katacari' />
			</div>
			<div style='float:left; '>
				&nbsp
				&nbsp
				<input type='submit' name='cari'  class='btn btn-primary' value='cari'/>
			</div>
		  
		  </form><br/>
	";
	
	echo "
	<a href='".URL."berita/tambah' class='btn btn-success'>Tambah Berita</a>
	<br/>
	<br/>
	<table id='example2' class='table table-bordered table-striped ' >
	<thead>
		<tr >
			<th>No</th>
			<th>Judul</th>
			<th>Isi</th>
			<th>Tindakan</th>
		</tr>
		
	</thead>
	<tbody>
	" ; // header tabel
	
	
	
		$find = '';
	if(isset($_REQUEST['cari'])){
		$katacari = $_REQUEST['katacari'];
		$find="&cari=h928hsolkcvnbw21&katacari=$katacari";
			
			$semua =  $artikel->countCariArtikel($katacari);
			
		}
	
		$semua =  $artikel->countArtikel();
		
		$per_page = 17; // jumlah query per berita 
	
		$i= 1;
			
		$pages = ceil($semua / $per_page); // melihat total blok yang ada
		
		$page = (isset($_GET['hal']))?(int)$_GET['hal'] :1; // default page
		
		$start = ($page-1)*$per_page; //startnya 
		
		$no = ($page-1)*$per_page +1; // menentukan asending nomor tiap paging
		
		$berita = (isset($_REQUEST['cari']))?$artikel->cariArtikel($katacari,$start,$per_page):$artikel->getArtikel($start,$per_page); //logika inti : kalo cari terisi maka ke method cari artikel 
		
		if($pages==0){echo "Data tidak ditemukan"; }
	
	foreach($berita as $berita){	
		
		echo "
		<tr>
			<td>".$no."</td>

			<td>".stripslashes (htmlspecialchars ($berita['judul']))."</td>

			<td>".substr(strip_tags($berita['isi']),0,110)."</td>

		
		<td>
			<div class='btn-group'>
				<button data-toggle='dropdown' class='btn btn-small btn-success dropdown-toggle'>Tindakan <span class='caret'></span></button>
				 <ul class='dropdown-menu'>
					 <li><a href='".URL."berita/edit/".$berita['id']."'><i class=' icon-edit'></i>Edit </a></li>
					 <li><a href=\"javascript: hapusAlert('".$berita['id']."');\"><i class=' icon-trash'></i>Hapus</a></li>
				 </ul>
			</div>
		</td>
	
		</tr>
		
		";
		$no++;
	}

	echo "
	</tbody>	
	</table>";
echo "<center ><div class='pagination'><ul class='pagination pagination-small center m-t-none m-b-none'>";

			// $req = empty($_REQUEST['view'])?'':$_REQUEST['view']; // mencari kata carinya
			
			$root = URL.'berita';//$_SERVER['PHP_SELF']; //."?view=".$req."&"; 
			
			$blok = 10;
			$ini  = ceil($page/$blok);
			
			$mulai   =  ($blok * $ini) - ($blok-1);
			$selesai =  ($blok<=$pages)?($ini * $blok):$pages ;
			
			$kurang1 = $page -1;
			$tambah1 = $page +1 ;
			
			if($pages >=1 && $page<=$pages){

				echo ($page!=1 or empty($page))?"<li style='cursor:pointer;'  ><a href='$root&hal=$kurang1$find'> Back </a> </li> ":'';
				for($x=$mulai; $x<=$selesai; $x++)
					if($x==$page){
						
						echo "<li style='cursor:pointer;' class='active' > <a>$x</a></li> ";
						
					}
					else{ 
						echo "<li style='cursor:pointer;'  ><a href='$root?hal=$x$find'>$x</a></li> ";
					};
				
				echo ($page!=$pages)?"<li style='cursor:pointer;'  ><a href='$root?hal=$tambah1$find'> Next </a> </li> ":'';					
			};
	echo "</ul></div> </center>";

break;

case "tambah":
	echo "
	<h3> Tambah Berita </h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."tambah' method='POST' name='form' class='form-horizontal'>

				<div class='control-group'>
					<label class='control-label'>Judul</label>
					<div class='controls'>
					<input type='judul' class='form-control' name='judul' id='judul' placeholder='judul' required>
					
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Isi</label>
					<div class='controls'>
						<textarea name='isi' class='form-control'>
						</textarea>
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Gambar : </label>
					<div class='controls'>
					<label > *.png | *.jpg dibawah 1 MB (jika ada)</label>
						<input type='file' name='file' id='uploadFile' accept='image/*' >
					</div>
				</div>
		
				<div class='control-group'>
					<label class='control-label'>Kategori</label>
					<div class='controls'>
						<input list='kategori' name='kategori'>
						<datalist id='kategori' >
							<option value='berita'> Berita </option>
							<option value='umum'> Umum </option>
						</datalist>
					</div>
				</div>
			
				<label class='control-label'>  </label>
				<button type='submit' name='tambah' value='tambah' class='btn btn-primary'>Tambah Berita</button>
				<br/>
				<div id='imagePreview' style='width:400px; height:266px;'></div>
		</form>

	";

break;
	
case "edit":
	// var_dump($parameter);
	if(filter_var($parameter, FILTER_VALIDATE_INT)){
		// echo $angka = $libs->stringHtml('123123');
		// $nomor = '1231234ff&nbspdfkdjkjfd"nfjksldj';
	  // $coba = filter_var($angka, FILTER_VALIDATE_INT);
	  // $coba = filter_var($nomor, FILTER_SANITIZE_MAGIC_QUOTES);
	  // var_dump($angka);
	 $artikel = $artikel->getArtikelById($parameter);
	
	echo "
	<h3>Edit Berita </h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal' >
				<div class='control-group'>
					<label class='control-label'>Judul</label>
					<div class='controls'>
						<input type='text' class='form-control' name='judul' id='judul' placeholder='judul' value='".$artikel['judul']."' required>
						<input type='hidden' class='form-control' name='id' value='".$artikel['id']."' >
						<input type='hidden' class='form-control' name='gambar'   value='".$artikel['gambar']."' >
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Konten</label>
					<div class='controls'>
						<textarea  name='isi' class='form-control'>
		
							".$artikel['isi']."
						
						</textarea>
					</div>
				</div>
				<div class='control-group'>
					<label class='control-label'>Gambar :</label>
					<div class='controls'>
						<label >*.png | *.jpg dibawah 1 MB (<b color='red'>Jika tidak ada perubahan gambar harap dikosongkan </b>)</label>
						<input type='file' value='asdasd' name='file' id='uploadFile' accept='image/*' >
						 
					</div>
				</div>
				
				<div class='control-group'>
					<label class='control-label'>Kategori</label>
					<div class='controls'>
					<input list='kategori' name='kategori' value='".$artikel['kategori']."'>
					<datalist id='kategori' >
						<option value='berita'> Berita </option>
						<option value='umum'> Umum </option>
					</datalist>
					</div>
				</div>
			<div class='box-footer'>
			
				<label class='control-label'> </label>
				
				<div class='controls'>
					<button type='submit' name='tambah' value='tambah' class='btn btn-primary'>Submit</button>
				</div>
			</div>
			<br/>
			
				<label class='control-label'> </label>

				<div id='imagePreview' style='width:400px; height:266px;'></div>
				
				<img style='width:400px; height:266px; float:left;' src='../../../images/content/news/".$artikel['gambar']."' />

			
		</form>

	";

break;

}else{
		header("location:".URL."berita");
		return false;
	}
}

?>
<script type='text/javascript'> 
 function hapusAlert(iddokumen){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ //modul/mod_berita/beritaAksi.php?act=berita&page=hapus&id=$berita[id]
			location.href="<?php echo $aksi."hapus" ;?>&id="+ iddokumen;
		}else{
			return false;
		}
	};

</script>

<?php 


echo " 		</div>
		</div>
    </div>";//end of wrapper
endif;
?>
<?php 
session_start();
if(isset($_GET['model'])): // for secure
ob_start();
date_default_timezone_set('Asia/Jakarta');
require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];
$model ;

// echo $method;
// echo $parameter;

if($model=='event' AND $method=='tambah' ){
	
	// var_dump($_FILES);
 	if(isset($_POST['tambah'])){
		$judul = $_POST['judul'];
		$judul = filter_var(strip_tags($judul), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		$konten = trim($_POST['isi']);
		$tanggal_mulai = (date('Y-m-d' ,strtotime($_POST['mulai'])));
		$tanggal_selesai = (date('Y-m-d',strtotime($_POST['selesai'])));
		$tempat = $_POST['tempat'];
		$long = $_POST['long'];
		$lat = $_POST['lat'];
		$link = $libs->changeLink($judul);
		$penulis = $_SESSION['username'];
		if(isset($_FILES['file']['name'])){

			// $gambar = $libs->uploadImage($_FILES['file']);	
			$gambar = $libs->uploadImageToFolder('../../../images/content/programs/',$_FILES['file']);	
			
		}

			 $event->insertEvent($judul,$link,$konten,$gambar,$penulis,$tanggal_mulai,$tanggal_selesai,$tempat,$long,$lat,$penulis); // method penyimpanan

		echo"<script> alert('Menambah data'); </script>";
		
	}
 
 // return false;
 }

if ($model=='event' AND $method=='edit' ){
	
	if(isset($_POST['tambah'])){

		$id = $_POST['id'];

		$gambar = $_POST['gambar'];
		
		$judul = $_POST['judul'];
		$judul = filter_var(strip_tags($judul), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		$konten = trim($_POST['isi']);
		$tanggal_mulai = (date('Y-m-d' ,strtotime($_POST['mulai'])));
		$tanggal_selesai = (date('Y-m-d',strtotime($_POST['selesai'])));
		$tempat = $_POST['tempat'];
		$long = $_POST['long'];
		$lat = $_POST['lat'];
		$link = $libs->changeLink($judul);
		$penulis = $_SESSION['username'];
		
		
		if(isset($_FILES['file']['name'])){
			$libs->hapusFile($gambar);
			
			// $gambar = $libs->uploadImage($_FILES['file']);	
			$gambar = $libs->uploadImageToFolder('../../../images/content/programs/',$_FILES['file']);	
		}
		
		$event->updateEvent($judul,$link,$konten,$gambar,$penulis,$tanggal_mulai,$tanggal_selesai,$tempat,$long,$lat,$penulis,$id); // method penyimpanan
			echo"<script> alert('Mengubah data'); </script>";
	}
}	
if($model=='event' AND $method=='hapus' ){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $event->getEventById($id);
	
	// $libs->hapusFile($data['gambar']);
		
	$libs->hapusGambarSpesific('../../../images/content/programs/',$data['gambar']);
	
	$event->deleteEvent($id);

 echo"<script> alert('Menghapus data'); </script>";
}
 header("location:".URL."event");
 
 endif;
?>
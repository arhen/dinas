<?php 
if(isset($method)):
date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/event_control/event_control.php?model=event&method="; // halaman untuk eksekusi


echo "
<div class='row-fluid'>
	<div class='span12'>
		<!-- BEGIN BASIC PORTLET-->
		<div class='widget green'>
			<div class='widget-title'>
				<h4><i class='icon-reorder'></i> Agenda </h4>
			<div class='actions'>
			   
			</div>
			
		</div>
		<div class='widget-body'>
";


// var_dump($method);
switch($method){

default :
	echo "
		<form  method='get' action=''>
			<div style='float:left; '>
				<input  class='form-control col-md-4' style='width:250px;' type='text' name='katacari' />
			</div>
			<div style='float:left; '>
				&nbsp
				&nbsp
				<input type='submit' name='cari'  class='btn btn-primary' value='cari'/>
			</div>
		  
		  </form><br/>
	";
	
	echo "
	<a href='".URL."event/tambah' class='btn btn-success'>Tambah Agenda</a>
	<br/>
	<br/>
	<table id='example2' class='table table-bordered table-striped ' >
	<thead>
		<tr >
			<th>No</th>
			<th>Judul</th>
			<th>Isi</th>
			<th>Tindakan</th>
		</tr>
		
	</thead>
	<tbody>
	" ; // header tabel
	
	
	
		$find = '';
	if(isset($_REQUEST['cari'])){
		$katacari = $_REQUEST['katacari'];
		$find="&cari=h928hsolkcvnbw21&katacari=$katacari";
			
			$semua =  $event->countCariEvent($katacari);
			
		}
	
		$semua =  $event->countEvent();
		
		$per_page = 17; // jumlah query per event 
	
		$i= 1;
			
		$pages = ceil($semua / $per_page); // melihat total blok yang ada
		
		$page = (isset($_GET['hal']))?(int)$_GET['hal'] :1; // default page
		
		$start = ($page-1)*$per_page; //startnya 
		
		$no = ($page-1)*$per_page +1; // menentukan asending nomor tiap paging
		
		$event = (isset($_REQUEST['cari']))?$event->cariEvent($katacari,$start,$per_page):$event->getEvent($start,$per_page); //logika inti : kalo cari terisi maka ke method cari event 
		
		if($pages==0){echo "Data tidak ditemukan"; }
	
	foreach($event as $event){	
		
		echo "
		<tr>
			<td>".$no."</td>

			<td>".stripslashes (htmlspecialchars ($event['judul']))."</td>

			<td>".substr(strip_tags($event['konten']),0,110)."</td>

		
		<td>
		
		<div class='btn-group'>
												<button data-toggle='dropdown' class='btn btn-small btn-success dropdown-toggle'>Tindakan <span class='caret'></span></button>
													 <ul class='dropdown-menu'>
														 <li><a href='".URL."event/edit/".$event['id']."'><i class=' icon-edit'></i>Edit </a></li>
														 <li><a href=\"javascript: hapusAlert('".$event['id']."');\"><i class=' icon-trash'></i>Hapus</a></li>
														
													 </ul>
												 </div>
		
		</td>
	
		</tr>
		
		";
		$no++;
	}
	
	echo "
	</tbody>	
	</table>

	";
	echo "<center >
		<div class='pagination'>
	<ul class='pagination pagination-small center m-t-none m-b-none'>";

			// $req = empty($_REQUEST['view'])?'':$_REQUEST['view']; // mencari kata carinya
			
			$root = URL.'event';//$_SERVER['PHP_SELF']; //."?view=".$req."&"; 
			
			$blok = 10;
			$ini  = ceil($page/$blok);
			
			$mulai   =  ($blok * $ini) - ($blok-1);
			$selesai =  ($blok<=$pages)?($ini * $blok):$pages ;
			
			$kurang1 = $page -1;
			$tambah1 = $page +1 ;
			
			if($pages >=1 && $page<=$pages){

				echo ($page!=1 or empty($page))?"<li style='cursor:pointer;'  ><a href='$root&hal=$kurang1$find'> Back </a> </li> ":'';
				for($x=$mulai; $x<=$selesai; $x++)
					if($x==$page){
						
						echo "<li style='cursor:pointer;' class='active' > <a>$x</a></li> ";
					}
					else{ 
						echo "<li style='cursor:pointer;'  ><a href='$root?hal=$x$find'>$x</a></li> ";
					};
				
				echo ($page!=$pages)?"<li style='cursor:pointer;'  ><a href='$root?hal=$tambah1$find'> Next </a> </li> ":'';
									
				
			};
	echo "</ul> </div></center>";

	break;

case "tambah":
	echo "
	<h3> Tambah Agenda</h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."tambah' method='POST' name='form' class='form-horizontal'>
					
				<div class='control-group'>
						<label class='control-label'>Nama Agenda</label>
					<div class='controls'>
						<input type='judul' class='form-control' name='judul' id='Nama Agenda' placeholder='Nama Agenda' required>
					</div>
				</div>
				
				<div class='control-group'>
						<label class='control-label'> Konten</label>
					<div class='controls'>
						<textarea name='isi' class='form-control'> </textarea>
					</div>
				</div>
				<div class='control-group'>
						<label class='control-label'>Gambar :</label>
					<div class='controls'>
						<label class=''>*.png | *.jpg dibawah 1 MB (jika ada)</label>
						<input type='file' name='file' id='uploadFile' accept='image/*' >
					</div>
					 
				</div>
			
				<div class='control-group'>
						<label class='control-label' >Tanggal Mulai</label>
					<div class='controls'>
						<input type='text'  id='dp1' name='mulai' value='".date("d-m-Y")."'>
					</div>
				</div>
				<div class='control-group'>
						<label class='control-label' >Tanggal Selesai</label>
					<div class='controls'>
						<input type='text' id='dp2' name='selesai' value='".date("d-m-Y")."'>
					</div>
				</div>
				<div class='control-group'>
						<label class='control-label'>Tempat</label>
					<div class='controls'>
						<input type='text'  name='tempat' >
					</div>
				</div>
				<div class='control-group'>
						<label class='control-label'> </label>
					<div class='controls'>
						<fieldset class='gllpLatlonPicker'>
							<div class='gllpMap'>Google Maps</div>
							Latitude : <input type='text' name='lat' class='gllpLatitude' value='-5.133439'/>
							Longitude : <input type='text' name='long' class='gllpLongitude' value='119.407761'/>
							<input type='hidden' name='zoom' class='gllpZoom' value='16'/>
						</fieldset>
					</div>
				</div>
			<br/>
			<div class='control-group'>
					<label class='control-label'> </label>
				<div class='controls'>
					<button type='submit' name='tambah' value='tambah agenda' class='btn btn-primary'> Buat </button>
				</div>
			</div>
				<div id='imagePreview' style='width:400px; height:366px;'></div>
		</form>

	";

break;
	
case "edit":
	// var_dump($parameter);
	if(filter_var($parameter, FILTER_VALIDATE_INT)){
		// echo $angka = $libs->stringHtml('123123');
		// $nomor = '1231234ff&nbspdfkdjkjfd"nfjksldj';
	  // $coba = filter_var($angka, FILTER_VALIDATE_INT);
	  // $coba = filter_var($nomor, FILTER_SANITIZE_MAGIC_QUOTES);
	  // var_dump($angka);
	 $event = $event->getEventById($parameter);
	
	echo "
	<h3> Edit Agenda </h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal'>
							
			<div class='control-group'>
					<label class='control-label'>Nama Agenda</label>
				<div class='controls'>
					<input type='text' class='form-control' value='".$event['judul']."' name='judul' id='judul' placeholder='judul' required>
					<input type='hidden' class='form-control' value='".$event['id']."' name='id'  required>
					<input type='hidden' class='form-control' value='".$event['gambar']."' name='gambar'   required>
				</div>
			</div>
							
			<div class='control-group'>
					<label class='control-label'>Konten</label>
				<div class='controls'>
					<textarea name='isi'class='form-control'>
					
						".$event['konten']."
					
					</textarea>
				</div>
			</div>
						
			<div class='control-group'>
					<label class='control-label'>Gambar</label>
				<div class='controls'>
					<label >Gambar : *.png | *.jpg dibawah 1 MB (jika ada)</label>
					<input type='file' name='file' id='uploadFile' accept='image/*' >
				</div>
			</div>
				
			<div class='control-group'>
					<label class='control-label'>Tanggal Mulai</label>
				<div class='controls'>
					<input type='text' id='dp1'  value='".date('d-m-Y',strtotime($event['tanggal_mulai']))."' name='mulai' value='".date("d/m/Y")."'>
				</div>
			</div>
						
			<div class='control-group'>
					<label class='control-label'>Tanggal Selesai</label>
				<div class='controls'>
					<input type='text' id='dp2' value='".date('d-m-Y',strtotime($event['tanggal_selesai']))."' name='selesai' value='".date("d/m/Y")."'>
				</div>
			</div>
				
			<div class='control-group'>
					<label class='control-label'>Tempat</label>
				<div class='controls'>
					<input type='text'  name='tempat' value='".$event['tempat']."' >
				</div>
			</div>	
			<div class='control-group'>
					<label class='control-label'> </label>
				<div class='controls'>
					<fieldset class='gllpLatlonPicker'>
							
						<label class='control-label'> </label>
						<div class='gllpMap'>Google Maps</div>
						Latitude : <input type='text' name='lat' class='gllpLatitude'value='".$event['latitude']."' />
						Longitude : <input type='text' name='long' class='gllpLongitude' value='".$event['longitude']."' />
			
						<input type='hidden' name='zoom' class='gllpZoom' value='16'/>
					
					</fieldset>
				</div>
			</div>
			
			<div class='control-group'>
					<label class='control-label'> </label>
				<div class='controls'>
					<button type='submit' name='tambah' value='tambah' class='btn btn-primary'>Submit</button>
				</div>
			</div>
				
				<div id='imagePreview' style='width:400px; height:300px;'></div>
				<br/>
				<img style='width:400px; height:300px; float:left;' src='".ROOT."images/content/programs/".$event['gambar']."'>
		</form>

	";

break;

}else{
		header("location:".URL."halaman");
		return false;
	}
}

?>
<script type='text/javascript'> 
 function hapusAlert(iddokumen){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ 
			location.href="<?php echo $aksi."hapus" ;?>&id="+ iddokumen;
		}else{
			return false;
		}
	};

</script>

<?php 

echo "
		</div>
	</div>
</div>
";
endif;
?>
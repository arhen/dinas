<?php 
if(isset($method)):

date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/galeri_control/galeri_control.php?model=galeri&method="; // galeri untuk eksekusi


// var_dump($method);
switch($method){

default :
	echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget orange'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Galeri</h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>";

$acak = date('dm').mt_rand(1,17).mt_rand(99,300);
	echo "
	<a href='".URL."galeri/tambah/".$acak."' class='btn btn-primary'>Tambah Album</a>
	<br/>
	<br/>
	<table id='example2' class='table table-bordered table-striped ' >
	<thead>
		<tr >
			<th>No</th>
			<th>Nama Album</th>
			<th>Tanggal Pembuatan</th>
			<th>Tindakan</th>
		</tr>
		
	</thead>
	<tbody>
	" ; // header tabel

	$semua =  $galeri->countGaleri();
	
		$per_page = 17; // jumlah query per halaman 
	
	$i= 1;
			
				$pages = ceil($semua / $per_page); // melihat total blok yang ada
				
				$page = (isset($_GET['hal']))?(int)$_GET['hal'] :1; // default page
				
				$start = ($page-1)*$per_page; //startnya 
				
				$no = ($page-1)*$per_page +1; // menentukan asending nomor tiap galeri paging
				
				$album = $galeri->getGaleri($start,$per_page); //menghitung 
				
				if($pages==0){echo "Data tidak ditemukan"; }
	
	
	
	
	foreach($album as $album){	
		
		echo "
		<tr>
		
			<td>".$no."</td>

			<td><a href='".URL."galeri/detail/".$album['id']."' class='btn btn-small btn-warning'>".stripslashes (htmlspecialchars($album['nama']))."</a></td>

			<td>".date("m-d-Y", strtotime($album['tanggal']))."</td>

			<td> ";
			if($album['nama'] != 'GAMBAR UTAMA'){
			echo "
				 <div class='btn-group'>
					<button data-toggle='dropdown' class='btn btn-small btn-success dropdown-toggle'>Tindakan <span class='caret'></span></button>
					 <ul class='dropdown-menu'>
						 <li><a href='".URL."galeri/edit/".$album['id']."'><i class=' icon-edit'></i>Edit </a></li>
						 <li><a href=\"javascript: hapusAlert('".$album['id']."');\" ><i class=' icon-trash'></i>Hapus</a></li>
						
					 </ul>
				 </div>
				 ";
			}
			echo"
			</td>
		</tr>
		
		";
		$no++;
	}
	echo "
	</tbody>	
	</table>
	";
	
	echo "<center><div class='pagination'><ul class='pagination pagination-small center m-t-none m-b-none'>";

			// $req = empty($_REQUEST['view'])?'':$_REQUEST['view']; // mencari kata carinya
			
			$root = URL.'galeri';//$_SERVER['PHP_SELF']; //."?view=".$req."&"; 
			
			$blok = 10;
			$ini  = ceil($page/$blok);
			
			$mulai   =  ($blok * $ini) - ($blok-1);
			$selesai =  ($blok<=$pages)?($ini * $blok):$pages ;
			
			$kurang1 = $page -1;
			$tambah1 = $page +1 ;
			
			if($pages >=1 && $page<=$pages){

				echo ($page!=1 or empty($page))?"<li style='cursor:pointer;'  ><a href='$root&hal=$kurang1'> Back </a> </li> ":'';
				for($x=$mulai; $x<=$selesai; $x++)
					if($x==$page){
						
						echo "<li style='cursor:pointer;' class='active'> <a >$x</a> </li> ";
						
					}
					else{ 
						echo "<li style='cursor:pointer;'  ><a href='$root?hal=$x'>$x</a></li> ";
					};
				
				echo ($page!=$pages)?"<li style='cursor:pointer;'  ><a href='$root?hal=$tambah1'> Next </a> </li> ":'';
									
				
			};
	echo "</ul> </div> </center>
	
		</div>
	</div>
</div>
	";
	
	
	break;

case "tambah":

 $id= $parameter;
echo "
<div class='row-fluid'>
			<div class='span12'>
				<!-- BEGIN BASIC PORTLET-->
				<div class='widget orange'>
					<div class='widget-title'>
						<h4><i class='icon-reorder'></i> Galeri</h4>
					<div class='actions'>
					   
					</div>
					
				</div>
				<div class='widget-body'>";
		echo "
			<form role='form' enctype='multipart/form-data' action='".$aksi."tambah' method='POST' name='form' class='form-horizontal'>
				<div class='control-group'>											
					<label class='control-label' >Nama Album</label>
					<div class='controls'>
						<input type='text' class='form-control' name='nama' id='nama' placeholder='nama' required />
					</div> <!-- /controls -->				
				</div> <!-- /control-group -->
				
				<div class='control-group'>											
					<label class='control-label' > </label>
					<div class='controls'>
						<button type='submit' name='submit' value='Tambah' class='btn btn-primary'>Submit</button>
					</div> <!-- /controls -->				
				</div> <!-- /control-group -->	
			</div> 
		</div> 
	</div> 
					
</form>";

break;

case "detail";

echo "<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget blue'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Galeri</h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>";

	if(filter_var($parameter, FILTER_VALIDATE_INT)){
	$album  = $galeri->getGaleriById($parameter);
	
	echo "<h2>Album : ".$album['nama']."</h2>";		
			
	echo "<form role='form' class='form-horizontal' enctype='multipart/form-data' action='".$aksi."file_tambah' method='POST' name='form' class='form-horizontal'>	
		<div class='control-group'>											
			<label class='control-label' >Keterangan Gambar</label>
			<div class='controls'>
				<input type='text' name='keterangan' /> <br/>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->

		<div class='control-group'>											
			<label class='control-label' >Gambar</label>
			<div class='controls'>
				<b>*Format Gambar hanya png dan jpg<br/></b>
				<input type='file' name='file' />  <br/>	
				<input type='hidden' class='form-control' value='".$parameter."' name='id' />
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
			
		
		<div class='control-group'>											
			<label class='control-label' ></label>
			<div class='controls'>
				<input type='submit' name='submit' value='Tambahkan'  class='btn btn-success'>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->
			
		
	
		
	</form><br/> ";
	
	echo "<table class='table '>
			<tr>
				<th>No</th>
				<th>Gambar</th>
				<th>Keterangan</th>
				<th></th>
			</tr>
	
		";
	$file = $galeri->getDetail($parameter);
	
	$no = 1;
	foreach($file as $file){
	echo "<tr> 
			<td>".$no."
			</td>
			<td>";
			
			echo "<img src='".ROOT."images/content/gallery/".$file['nama_file']."' style='width:40px; height:36px;' />";
	
	echo "	</td>
			<td>
				".$file['keterangan'] ."
			</td>
			<td>
				<a href='".$aksi."file_hapus&id=".$file['id'] ."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a>
			</td>
		 </tr>";
	$no++;
	}
	
		echo "</table>";
	
	}
		echo "</div>
		</div>
	</div>";
break ;
case "edit":
	// var_dump($parameter);
	if(filter_var($parameter, FILTER_VALIDATE_INT)){
	
	$detail = $galeri->getGaleriById($parameter);

		
	echo "
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal'>
			<div class='control-group'>											
				<label class='control-label' >Nama Album</label>
				<div class='controls'>
					<input type='text' class='form-control' name='nama' id='nama' placeholder='nama' required value='".$detail['nama']."' />
					<input type='hidden' class='form-control' name='id' value='".$detail['id']."' />
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
			
			<div class='control-group'>											
				<label class='control-label' > </label>
				<div class='controls'>
					<button type='submit' name='submit' value='Ubah' class='btn btn-primary'>Submit</button>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->	
					
		</form>";

break;

}else{
		header("location:".URL."galeri");
		return false;
	}
}

?>



<script type='text/javascript'> 
 function hapusAlert(iddokumen){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ //modul/mod_berita/beritaAksi.php?act=berita&page=hapus&id=$berita[id]
			location.href="<?php echo $aksi."hapus" ;?>&id="+ iddokumen;
		}else{
			return false;
		}
	};

</script>

<?php endif;?>
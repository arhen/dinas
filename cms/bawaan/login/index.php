<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>Login</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="../css/style.css" rel="stylesheet" />
   <link href="../css/style-responsive.css" rel="stylesheet" />
   <link href="../css/style-default.css" rel="stylesheet" id="style_color" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="error-404">
    <div class="lock-header">
        <!-- BEGIN LOGO -->
        <a class="center" id="logo"  >
            <img class="center" alt="logo"  src="../img/mks.png">
			<h2 style='color:white;'> <b>Halaman Admin</b></h2>
        </a>
        <!-- END LOGO -->
    </div>
	
    <div class="login-wrap">
        <div class="metro single-size red">
            <div class="locked">
                <i class="icon-lock"></i>
                <span>Login</span>
            </div>
        </div>
        <div class="metro double-size green">
            <form id='login' action="" method='POST' onsubmit='return sub();'>
                <div class="input-append lock-input">
                   <input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" autofocus="autofocus" />
                </div>
           
        </div>
        <div class="metro double-size yellow">
            
                <div class="input-append lock-input">
                    <input type="password" id='password' name='pass' class="" placeholder="Password">
					
					
                </div><br/>
				<label style='font-color:red;'>
					
			<?php 
				// require 'core/class.php';
				 if(isset($_POST['login'])){
					 
				require("../model/class.php"); 
				 $uss = empty($_POST['username'])?'':htmlentities($_POST['username'],ENT_QUOTES,'utf-8');
				 $pas = empty($_POST['pass'])?'':filter_var($_POST['pass'], FILTER_SANITIZE_STRING);
				 $username = filter_var($uss,FILTER_SANITIZE_STRING);
				 $username = filter_var($uss,FILTER_SANITIZE_MAGIC_QUOTES);
				 
				 
				 $pass     = filter_var($pas,FILTER_SANITIZE_MAGIC_QUOTES);
				// pastikan username dan password adalah berupa huruf atau angka.

				//if (password_verify("2d8cc94a8c8b5ca7400969c5b2e572c1", '$2y$10$ae2xru0nX494I8pnCZlj2Og/eeBCP.eZeHjF/dOiUr582FTECokpK')) {}// membandingkan pass yang di client dan hash di server
				$user = $users->getUserByUsername($username);

					$count=0;

				if (password_verify($pass, $user['password'])) {
					// Verified echo
					
				echo 	$count=$users->authUser($username);// login
					

				}	

				// Apabila username dan password ditemukan
				if ($count > 0){
					  @session_start();
					  // include "timeout.php";
					  $_SESSION['username']    		= $user['username'];
					  $_SESSION['nama_lengkap'] 	= $user['nama_lengkap'];
					  $_SESSION['password']    		= $user['password'];
					  $_SESSION['level']    		= $user['level'];
					  $_SESSION['nopeg']   			= $user['nip'];

					  // session timeout
					  $_SESSION['login'] = 1;
					  
					  $libs->timer();
					
					  $_SESSION['timeout'];
					  
					  $libs->cek_login();
						// echo "<br/>";

						$sid_lama = session_id();
						
						session_regenerate_id();

						// echo "<br/>";

						$sid_baru = session_id();
						
						$users->setSessionUser($username,$sid_baru); // update session id pada user

						header("location:../");
					}
					else{
						echo "Username dan password tidak sesuai";
					}

				 }
			
			?>
				</label>
        </div>
        <div class="metro single-size terques login">
        
                <button type="submit" name='login' value='login'  class="btn login-btn">
                    Login
                    <i class=" icon-long-arrow-right"></i>
                </button>
            </form>
        </div>
        
        
        <div class="login-footer">
            <div class="remember-hint pull-left">
                
            </div>
            <div class="forgot-hint pull-right">
                
        </div>
    </div>
	</div>
</body>
<!-- END BODY -->

	<script type="text/javascript" src="../js/jquery.min.js"></script>
    <script src="../js/md5.js"></script>
	  
	  <script type="text/javascript">
	  $(function() {
		
		function sub(){
		var password = $('#password').val();
		var md5 = CryptoJS.MD5(CryptoJS.MD5(password)+CryptoJS.MD5(password));
		
		// var aaa = $('#hiddenpassword').val(md5);
		
		var ssss = document.getElementById("password").value = md5;
		// alert(ssss);
		return true;
	  
	  };
		
		$("#login").on("submit", function()
		{
			sub();
			
		});
	});

	  
	</script>
</html>
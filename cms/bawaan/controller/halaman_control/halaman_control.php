<?php 
// session_start();
if($_REQUEST['model']): // for secure single file
ob_start();
date_default_timezone_set('Asia/Jakarta');

require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];
$model ;

// echo $method;
// echo $parameter;

if($model=='halaman' AND $method=='tambah' ){
	
 	if(isset($_POST['tambah'])){
		$judul = $_POST['judul'];

		$isi = $_POST['isi'];
		
		$id = $_POST['id'];

		$judul = filter_var($judul, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		// $isi = $libs->stringHtml($isi);
		$judul = strtoupper($judul);
		
 		$link = $libs->changeLink($judul);

		if(isset($_FILES['file'])){
		
		$nama_baru = $libs->uploadImage($_FILES['file']);	
		
		}

		$halaman->insertHalaman($judul,$link,$isi,$nama_baru,$id); // method penyimpanan

		echo"<script> alert('Menambah data'); </script>";
		
		 header("location:".URL."halaman");
	}
 
 // return false;
 }
 
if($model=='halaman' AND $method=='file_tambah' ){
	
 	if(isset($_POST['submit'])){

		$id_halaman = $_POST['id'];
		
		$judul = $_POST['judul'];

		if(isset($_FILES['file'])){
		
			$nama_file = $libs->uploadPdf($_FILES['file']);	
			if($nama_file != '' or $nama_file != false){
				
				$halaman->insetFileHalaman($id_halaman,$judul,$nama_file); // method penyimpanan
				
				echo"<script> alert('Manambahkan File'); </script>";

			}else{
				echo"<script> alert('Tidak dapat menambah file, harap periksa file dan coba lagi'); </script>";
				
			}

		}
		
		echo"<script> window.history.back(); </script>";
	}
 
 }
 
if($model=='halaman' AND $method=='uraian_tambah' ){
	
 	if(isset($_POST['submit'])){
	
		$nama = $_POST['nama'];
		
		$id = $_POST['id'];

	
	
		$halaman->insertUraian($nama,$id); // method penyimpanan

		echo"<script> window.history.back(); </script>";
		
	}
 
 // return false;
 }

if ($model=='halaman' AND $method=='edit' ){
	
	if(isset($_POST['tambah'])){
		$judul = $_POST['judul'];

		$id = $_POST['id'];

		$gbr = $_POST['gambar'];
		
		// var_dump($gbr);
		$isi = $_POST['isi'];

		$judul = filter_var($judul, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		
		// $isi = filter_var($isi, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		// $isi = $libs->stringHtml($isi);
		
		$link = $libs->changeLink($judul);
		
		$nama_baru = $gbr;
		
		if(!empty($_FILES['file']['name'])){
			
			// $root = $_SERVER['DOCUMENT_ROOT'].'dinas/upload/';
			// $root = '../../../upload/';
			// var_dump($root);
			
		 	$nama_baru = $libs->uploadImage($_FILES['file']);	
		
		}
		
		$halaman->updateHalaman($judul,$link,$isi,$nama_baru,$id); // method penyimpanan
		
		header("location:".URL."halaman");
	}
}	

if($model=='halaman' AND $method=='uraian_hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $halaman->deleteUraian($id);
	
	echo"<script> window.history.back(); </script>";

}

if($model=='halaman' AND $method=='hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $halaman->getHalamanById($id);
	
	$libs->hapusGambarUpload($data['gambar_halaman']);
	
	$halaman->deleteHalaman($id);

	 echo"<script> alert('Menghapus data'); </script>";
	 
	 header("location:".URL."halaman");
}

if($model=='halaman' AND $method=='file_hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $halaman->getFileById($id);
	
	$libs->deleteFile($data['nama_file']);
	
	$halaman->deleteFile($id);
	echo"<script> window.history.back(); </script>";


}


 // header("location:".URL."halaman");
 
 endif;
?>
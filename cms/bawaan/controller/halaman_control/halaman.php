<?php 
if(isset($method)):
echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget orange'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Halaman</h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>
";



date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/halaman_control/halaman_control.php?model=halaman&method="; // halaman untuk eksekusi


// var_dump($method);
switch($method){

default :


$acak = date('dm').mt_rand(1,17).mt_rand(99,300);

echo "

                            <div class='widget-body'>
							 <a href='".URL."halaman/tambah/".$acak."' class='btn btn-primary'><i class='icon-plus'></i>Tambah Halaman</a>
							 <br/>
							 <br/>
                                <table class='table table-striped table-bordered table-advance table-hover'>
                                    <thead>
                                    <tr>
                                        <th style='width:5%;'>No</th>
                                        <th class='hidden-phone'>Judul</th>
                                        <th> Konten</th>
                                        <th> Tindakan</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>";
						 
										$semua =  $halaman->countHalaman();
									
										$per_page = 23; // jumlah query per halaman 
									
										$i= 1;
							
										$pages = ceil($semua / $per_page); // melihat total blok yang ada
										
										$page = (isset($_GET['hal']))?(int)$_GET['hal'] :1; // default page
										
										$start = ($page-1)*$per_page; //startnya 
										
										$no = ($page-1)*$per_page +1; // menentukan asending nomor tiap halaman paging
										
										$halaman = $halaman->getHalaman($start,$per_page); //menghitung 
										
										if($pages==0){echo "Data tidak ditemukan"; }
							
							
										foreach($halaman as $halaman){
											
										echo "
										<tr>
											<td>".$no."</td>

											<td>".stripslashes (htmlspecialchars ($halaman['judul_halaman']))."</td>

											<td>".substr(strip_tags($halaman['konten_halaman']),0,110)."</td>

											<td>";
												if($halaman['atribut']!=1){
												echo "
												<div class='btn-group'>
												<button data-toggle='dropdown' class='btn btn-small btn-success dropdown-toggle'>Tindakan <span class='caret'></span></button>
													 <ul class='dropdown-menu'>
														 <li><a href='".URL."halaman/edit/".$halaman['id_halaman']."'><i class=' icon-edit'></i>Edit </a></li>
														 <li><a href=\"javascript: hapusAlert('".$halaman['id_halaman']."');\"><i class=' icon-trash'></i>Hapus</a></li>
														
													 </ul>
												 </div>
												 ";
												}
												 echo "
											</td>
										
										</tr>";
										$no++;
									}
	 
echo "                                                               
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END BASIC PORTLET-->
                   
	";

	echo "<center>
	<div class='pagination'>
	<ul class=''>";

			// $req = empty($_REQUEST['view'])?'':$_REQUEST['view']; // mencari kata carinya
			
			$root = URL.'halaman';//$_SERVER['PHP_SELF']; //."?view=".$req."&"; 
			
			$blok = 10;
			$ini  = ceil($page/$blok);
			
			$mulai   =  ($blok * $ini) - ($blok-1);
			$selesai =  ($blok<=$pages)?($ini * $blok):$pages ;
			
			$kurang1 = $page -1;
			$tambah1 = $page +1 ;
			
			if($pages >=1 && $page<=$pages){

				echo ($page!=1 or empty($page))?"<li style='cursor:pointer;'  ><a href='$root&hal=$kurang1'> Back </a> </li> ":'';
				for($x=$mulai; $x<=$selesai; $x++)
					if($x==$page){
						
						echo "<li style='cursor:pointer;' class='active'> <a >$x</a> </li> ";
						
					}
					else{ 
						echo "<li style='cursor:pointer;'  ><a href='$root?hal=$x'>$x</a></li> ";
					};
				
				echo ($page!=$pages)?"<li style='cursor:pointer;'  ><a href='$root?hal=$tambah1'> Next </a> </li> ":'';
									
				
			};
	echo "</ul></div> </center>";


	break;

case "tambah":

 $id= $parameter;
echo "<h3>Tambah Halaman</h3>";
	echo "<br/>
		<form role='form' enctype='multipart/form-data' action='".$aksi."tambah' method='POST' name='form' class='form-horizontal'>
			<div class='control-group'>											
				<label class='control-label' >Judul</label>
				<div class='controls'>
					<input type='text' class='form-control' name='judul' id='judul' placeholder='judul' required />
					<input type='hidden' class='form-control' value='".$id."' name='id' id='judul' placeholder='judul'  />
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->

			<div class='control-group'>											
				<label class='control-label' >Konten</label>
				<div class='controls'>
					<textarea name='isi' class='form-control'>
					</textarea>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
			
			<div class='control-group'>											
				<label class='control-label' >Gambar </label>
				<div class='controls'>
					<label > *.png | *.jpg dibawah 1 MB (jika ada) {Gambar disarankan memiliki tinggi <b color='red'>400px</b>}</label>
					<input type='file' name='file' id='uploadFile' accept='image/*' >
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
			
			<div class='control-group'>											
				<label class='control-label' > </label>
				<div class='controls'>
					<button type='submit' name='tambah' value='tambah' class='btn btn-primary'>Submit</button>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->	
					
		</form>";

echo "		<div class='span2'></div >
			<div class='control-group'>											
				
				<div class='controls'>
					<div class='span6'>
						<!-- BEGIN TAB PORTLET-->
						<div class='widget widget-tabs'>
							<div class='widget-title'>
								<h4><i class='icon-reorder'></i> Informasi Tambahan</h4>
							</div>
								<div class='widget-body'>
									<div class='tabbable '>
										<ul class='nav nav-tabs'>
											<li><a href='#widget_tab2' data-toggle='tab'>File </a></li>
											<li class='active'><a href='#widget_tab1' data-toggle='tab'>Uraian</a></li>
										</ul>
										<div class='tab-content'>
											<div class='tab-pane active' id='widget_tab1'>
												<div >
												<form role='form' class='form-horizontal' enctype='multipart/form-data' action='".$aksi."uraian_tambah' method='POST' name='form' class='form-horizontal'>	
			
													Uraian : <input type='text' style='width:600px;' id='uraian' name='nama'>
													<input type='hidden' class='form-control' value='".$id."' name='id' />
													<input type='submit' name='submit' class='btn btn-success' value='tambahkan'>
												</form>";
													
				echo 					" <br/>
											<table class='table'>
										";
										
										$uraian = $halaman->getUraian($parameter);
								
										foreach($uraian as $uraian){
										echo "<tr> 
												<td>";
											echo "".$uraian['uraian'] ." ";
										
										echo "	</td>
												<td>
													<a href='".$aksi."uraian_hapus&id=".$uraian['id'] ."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a>
												</td>
											 </tr>";
										}
								echo "</table>";
											
										echo "	</div>	
										</div>
										<div class='tab-pane' id='widget_tab2'>
										   <form role='form' class='form-horizontal' enctype='multipart/form-data' action='".$aksi."file_tambah' method='POST' name='form' class='form-horizontal'>	
												Judul File :
												<input type='text' id='upl' name='judul' /> <br/> <br/>
												<input type='file' name='file' accept='application/pdf' />  <br/> <br/>
												<input type='hidden' class='form-control' value='".$parameter."' name='id' />
												<input type='submit' name='submit' value='Tambahkan'  class='btn btn-success'>
											</form><br/><table>"; 

									$file = $halaman->getFile($parameter);
								
										foreach($file as $file){
										echo "<tr> 
												<td style='width:300px;'>";
											echo "".$file['judul'] ." ";
										
										echo "	</td>
												<td>
													<a href='".$aksi."file_hapus&id=".$file['id'] ."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a>
												</td>
											 </tr>";
										}

							 echo "     </table></div>
									</div>
								</div>
							</div>
						</div>
                    <!-- END TAB PORTLET-->
                </div>
			</div> <!-- /controls -->				
		</div> <!-- /control-group -->

		<div class='control-group'>											
			<div id='imagePreview' style='width:900px; height:300px;'></div>		
		</div> <!-- /control-group -->
	";

break;
	
case "edit":

	// var_dump($parameter);
	if(filter_var($parameter, FILTER_VALIDATE_INT)){
	
	$id= $parameter;
		
	 $detail = $halaman->getHalamanById($parameter);

	echo "
	<h3> Edit Halaman</h3>
		<form role='form' enctype='multipart/form-data' action='".$aksi."edit' method='POST' name='form' class='form-horizontal'>
			
			<div class='control-group'>											
				<label class='control-label' >Judul</label>
				<div class='controls'>
					<input type='text' class='form-control' name='judul' id='judul' placeholder='judul' value='$detail[judul_halaman]' required>
					<input type='hidden' class='form-control' name='id' value=".$detail['id_halaman']."' >
					<input type='hidden' class='form-control' name='gambar'   value='".$detail['gambar_halaman']."' >
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->

			<div class='control-group'>											
				<label class='control-label' >Konten</label>
				<div class='controls'>
					<textarea  name='isi' class='form-control'>
	
						".$detail['konten_halaman']."
					
					</textarea>
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
				
			<div class='control-group'>											
				<label class='control-label' >Gambar</label>
				<div class='controls'>
					<label >Gambar : *.png | *.jpg dibawah 1 MB (<b color='red'>Jika tidak ada perubahan gambar harap dikosongkan </b>) {Gambar disarankan memiliki tinggi <b color='red'>400px</b>}</label>
					<input type='file' value='a' name='file' id='uploadFile' accept='image/*' id='uploadFile' >
					 
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->
		
			<div class='control-group'>											
				<label class='control-label' > </label>
				<div class='controls'>
					<button type='submit' name='tambah' value='tambah' class='btn btn-primary'>Submit</button>
					 
				</div> <!-- /controls -->				
			</div> <!-- /control-group -->

		</form>	";
echo "		<div class='span2'></div >
			<div class='control-group'>											
				
				<div class='controls'>
					<div class='span6'>
						<!-- BEGIN TAB PORTLET-->
						<div class='widget widget-tabs'>
							<div class='widget-title'>
								<h4><i class='icon-reorder'></i> Informasi Tambahan</h4>
							</div>
								<div class='widget-body'>
									<div class='tabbable '>
										<ul class='nav nav-tabs'>
											<li><a href='#widget_tab2' data-toggle='tab'>File </a></li>
											<li class='active'><a href='#widget_tab1' data-toggle='tab'>Uraian</a></li>
										</ul>
										<div class='tab-content'>
											<div class='tab-pane active' id='widget_tab1'>
												<div >
												<form role='form' class='form-horizontal' enctype='multipart/form-data' action='".$aksi."uraian_tambah' method='POST' name='form' class='form-horizontal'>	
			
													Uraian : <input type='text' style='width:600px;' id='uraian' name='nama'>
													<input type='hidden' class='form-control' value='".$id."' name='id' />
													<input type='submit' name='submit' class='btn btn-success' value='tambahkan'>
												</form>";
													
				echo 					" <br/>
											<table class='table'>
										";
										
										$uraian = $halaman->getUraian($parameter);
								
										foreach($uraian as $uraian){
										echo "<tr> 
												<td>";
											echo "".$uraian['uraian'] ." ";
										
										echo "	</td>
												<td>
													<a href='".$aksi."uraian_hapus&id=".$uraian['id'] ."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a>
												</td>
											 </tr>";
										}
								echo "</table>";
											
										echo "	</div>	
										</div>
										<div class='tab-pane' id='widget_tab2'>
										   <form role='form' class='form-horizontal' enctype='multipart/form-data' action='".$aksi."file_tambah' method='POST' name='form' class='form-horizontal'>	
												Judul File :
												<input type='text' id='upl' name='judul' /> <br/> <br/>
												<input type='file' name='file' accept='application/pdf' />  <br/> <br/>
												<input type='hidden' class='form-control' value='".$parameter."' name='id' />
												<input type='submit' name='submit' value='Tambahkan'  class='btn btn-success'>
											</form><br/><table>"; 

									$file = $halaman->getFile($parameter);
								
										foreach($file as $file){
										echo "<tr> 
												<td style='width:300px;'>";
											echo "".$file['judul'] ." ";
										
										echo "	</td>
												<td>
													<a href='".$aksi."file_hapus&id=".$file['id'] ."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a>
												</td>
											 </tr>";
										}

							 echo "     </table></div>
									</div>
								</div>
							</div>
						</div>
                    <!-- END TAB PORTLET-->
                </div>
		<div class='control-group'>";
					
		echo (!empty($detail['gambar_halaman']))?"<img src='".ROOT."upload/".$detail['gambar_halaman']."' style='width:900px; height:300px;'/>":'';				
					
		echo"		<div id='imagePreview' style='width:900px; height:300px;'></div>		
				</div> <!-- /control-group -->
		</div> <!-- /control-group -->
	</div> <!-- /controls -->						
		
		
	";

break;

}else{
		header("location:".URL."halaman");
		return false;
	}
}


echo " 		</div>
		</div>
    </div>";//end of wrapper
?>
<script type='text/javascript'> 
 
document.getElementById("uraian").value = "";

document.getElementById("upl").value = "";

 function hapusAlert(iddokumen){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ //modul/mod_berita/beritaAksi.php?act=berita&page=hapus&id=$berita[id]
			location.href="<?php echo $aksi."hapus" ;?>&id="+ iddokumen;
		}else{
			return false;
		}
	};

</script>

<?php endif;?>
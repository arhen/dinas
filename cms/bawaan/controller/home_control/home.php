<?php 
if(isset($method)):

date_default_timezone_set('Asia/Jakarta');
// include('../../lib');
$aksi = URL."controller/home_control/home_control.php?model=home&method="; // halaman untuk eksekusi


// var_dump($method);
switch($method){

default :
echo "

";
$running = $home->getRunningById();
echo '
			<div class="row-fluid">
				<div class="span12">
					<h3 class="page-title">
					   Home
					</h3>
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-th-large"></i> Shortcut Link </h4>
					   <span class="tools">
						   <a href="javascript:;" class="icon-chevron-down"></a>
						   <a href="javascript:;" class="icon-remove"></a>
					   </span>
						</div>
						<div class="widget-body">
							<!--BEGIN METRO STATES-->
							<div class="metro-nav">
								<div class="metro-nav-block nav-block-orange">
									<a data-original-title="" class="text-center" href="'.URL.'berita">
										<i class="icon-globe"></i>
										<div class="info"></div>
										<div class="status" class="text-center">Berita</div>
									</a>
								</div>
								<div class="metro-nav-block nav-block-green">
									<a data-original-title="" class="text-center" href="'.URL.'agenda">
										<i class="icon-th-list"></i>
										<div class="info"></div>
										<div class="status" class="text-center">Agenda</div>
									</a>
								</div>
								<div class="metro-nav-block nav-block-purple">
									<a data-original-title="" class="text-center" href="'.URL.'pengumuman">
										<i class="icon-bullhorn"></i>
										<div class="info"></div>
										<div class="status" class="text-center">Pengumuman</div>
									</a>
								</div>
								<div class="metro-nav-block nav-block-blue">
									<a data-original-title="" href="javascript:;">
										<i class="icon-eye-open"></i>
										<div class="info">+897</div>
										<div class="status" class="text-center">Unique Visitor</div>
									</a>
								</div>
								<div class="metro-nav-block nav-block-red">
									<a data-original-title="" class="text-center" href="'.URL.'galeri">
										<i class="icon-picture"></i>
										<div class="status" class="text-center" >Galery</div>
									</a>
								</div>
							</div>
							<div class="clearfix"></div>
							<!--END METRO STATES-->
						</div>
					</div>
				</div>
			</div>
';
echo "
			<div class='row-fluid'>
                <div class='span6 '>
                    <!-- BEGIN widget widget-->
                    <div class='widget purple'>
                        <div class='widget-title'>
                            <h4><i class='icon-reorder'></i> Running Text </h4>
                            <div class='actions'>
								<form role='form'  action='".$aksi."Runningedit' method='POST' name='form' >
								<button type='submit' value='edit' name='edit'  class = 'btn btn-primary' style='float:right; '> <i class='icon-pencil'></a></i>  Edit</button>
                            </div>
                        </div>
                        <div class='widget-body'>
                            <div class='portlet-scroll-2' tabindex='5003' style='overflow: hidden; outline: none; height:auto;'>
                              
								<!--<input type='text' style='width:900px;'value='".$running['konten']."' name='konten'/>-->
								<textarea  name='konten'>
									".$running['konten']."
								</textarea>
								
							  </form>
                            </div>
                        </div>
                    </div>
                    <!-- END widget widget-->
					
                </div>
                    <!-- END widget widget-->
					
				<div class='span6'>
					<!-- BEGIN BASIC PORTLET-->
					<div class='widget red'>
						<div class='widget-title'>
							<h4><i class='icon-reorder'></i> Nomor Penting </h4>
						<span class='tools'>
							<a href='javascript:;' class='icon-chevron-down'></a>
							<a href='javascript:;' class='icon-remove'></a>
						</span>
						</div>
						<div class='widget-body'>
							<table class='table table-striped'>
							<div class='widget-content'><br/>
              
								<thead>
								<tr>
									<th>No</th>
									<th>Nama Instantsi</th>
									<th>Nomor</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<form id='dashboadInsert' action='".$aksi."noTambah' method='post'>
								<tr>
									
									<td colspan='2'><input type='text' require name='nama' style='width:250px;' /></td>
									<td><input type='text' require name='nomor' style='width:70px;' /></td>
									<td><input type='submit' name='tambah' value='tambah' style='margin-top:-2px;' class = 'btn btn-primary'/></td>
								</tr>	
								</form>
									";
									$nomor  = $home->getNomorPenting();
									$no =1;
									foreach($nomor as $nomor){
										// echo "<label>".$nomor['nama']." | ".$nomor['nomor']." <a href='controller/home_control/home_control.php?model=home&method=hapus&id=".$nomor['id']."' class='del' role='".$nomor['id']."'>x </a></label> <br/>";

									echo "            <tr>
														<td> ".$no." </td>
														<td> ".$nomor['nama']." </td>
														<td> ".$nomor['nomor']."</td>
														<td class='td-actions' ><a href='controller/home_control/home_control.php?model=home&method=hapus&id=".$nomor['id']."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a></td>
													  </tr>";
													  $no++;
													  }
													  
									echo " 								
								</tbody>
							</table>
						</div>
					</div>
					<!-- END BASIC PORTLET-->
				</div>
            </div>
	
<!-- bawah -->	
	<div class='row-fluid'>
                <div class='span6 '>
                    <!-- BEGIN widget widget-->
                    <div class='widget purple'>
                        <div class='widget-title'>
                            <h4><i class='icon-reorder'></i> Badge </h4>
                            <div class='actions'>
                            </div>
                        </div>
                        <div class='widget-body'>
                            <div class='portlet-scroll-2' tabindex='5003' style='overflow: hidden; outline: none; height:auto;'>
                              <table class='table table-striped'>
							<div class='widget-content'><br/>
              
								<thead>
								<tr>
									<th>No</th>
									<th>Gambar</th>
									<th>Keterangan</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<form  enctype='multipart/form-data'  id='dashboadInsert' action='".$aksi."tambah_badge' method='post'>
								<tr>
									
									<td colspan='2'><input type='file' name='file' id='uploadFile' accept='image/*' style='width:200px;' /></td>
									<td ><input type='text' required name='keterangan' style='width:110px;' /></td>
									<td><input type='submit' name='badge' value='Tambah' style='margin-top:-2px;' class = 'btn btn-primary'/></td>
								</tr>	
								</form>
									";
									$badge  = $home->getBadge();
									$no =1;
									foreach($badge as $badge){
									echo "            <tr>
														<td> ".$no." </td>
														<td> <img src='".ROOT."upload/".$badge['nama_file']."' style='width:110px; height:120px;' /></td>
														<td> ".$badge['keterangan']."</td>
														<td class='td-actions' ><a href='".$aksi."hapus_badge&id=".$badge['id']."' class='btn btn-danger btn-small'><i class='btn-icon-only icon-remove'> </i></a></td>
													  </tr>";
													  $no++;
													  }
													  
									echo " 								
								</tbody>
							</table>
                            </div>
                        </div>
                    </div>
                    <!-- END widget widget-->
					
                </div><!-- Akhir widget -->		
			
        </div>

";



break;
}
?>

<?php endif;?>



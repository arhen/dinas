<?php 
// session_start();
if(isset($_REQUEST['model'])): // for secure single file
date_default_timezone_set('Asia/Jakarta');

require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

 $method=$_GET['method'];

if ($model=='home' AND $method=='Runningedit' ){
	
	if(isset($_POST['edit'])){
		
		$konten = strip_tags($_POST['konten']);

		$konten = filter_var(trim($konten), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		
		$home->updateRunning($konten); // method penyimpanan
	}
}
	
if ($model=='home' AND $method=='noTambah' ){
	if(isset($_POST['nama'])){
		
		$nama = $_POST['nama'];
		$nama = filter_var(trim($nama), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		
		$nomor = $_POST['nomor'];
		$nomor = filter_var(trim($nomor), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		
		echo $home->insertNomor($nama, $nomor); // method penyimpanan
		
		// return false;
	}
}	

if($model=='home' AND $method=='tambah_badge' ){
		
		if(isset($_POST['badge'])){
			
			$keterangan = $_POST['keterangan'];
			
		 	$nama_file = $libs->uploadImage($_FILES['file']);	
			
			if($nama_file != '' or $nama_file != false){

			$home->insertBadge($nama_file, $keterangan); // method penyimpanan
			
			echo"<script> alert('Menambah Gambar'); </script>";
			
			}else{
			
				echo"<script> alert('Gagal Mengupload Gambar'); </script>";
				
			}

			
		}
	 
	 // return false;
	 }
	 
if ($model=='home' AND $method=='noTambah' ){
	if(isset($_POST['nama'])){
		
		$nama = $_POST['nama'];
		$nama = filter_var(trim($nama), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		
		$nomor = $_POST['nomor'];
		$nomor = filter_var(trim($nomor), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		
		echo $home->insertNomor($nama, $nomor); // method penyimpanan
		
		// return false;
	}
}

if ($model=='home' AND $method=='hapus' ){

		$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
		
		$home->deleteNomor($id);
		

}	

if ($model=='home' AND $method=='hapus_badge' ){

		$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
		
		$home->deleteBadge($id);
		

}	
// var_dump($_POST);
 header("location:".URL." ");
 
 endif;
?>
<?php 
// session_start();
if($_REQUEST['model']): // for secure single file
ob_start();
date_default_timezone_set('Asia/Jakarta');

require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];
$model ;

// echo $method;
// echo $parameter;

if($model=='struktur_organisasi' AND $method=='tambah' ){
	
 	if(isset($_POST['submit'])){
		
		$nama = $_POST['nama'];
		
		$nama = filter_var($nama, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 

		$struktur_organisasi->insertStrukturOrganisasi($nama); // method penyimpanan

		echo"<script> alert('Menambah data'); </script>";
		
		 header("location:".URL."struktur_organisasi");
	}
 
 // return false;
 }
 
if($model=='struktur' AND $method=='edit' ){
	
 	if(isset($_POST['submit'])){
		
		$nama = $_POST['nama'];
		$id = $_POST['id'];
		
		$nama = filter_var(trim($nama), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		// $isi = $libs->stringHtml($isi);

 		$link = $libs->changeLink($nama);

		$struktur->updateGaleri($nama,$link,$id); // method penyimpanan

		echo"<script> alert('Mengubah data'); </script>";
		
		 header("location:".URL."struktur");
	}
 
 // return false;
 }
 
if($model=='struktur_organisasi' AND $method=='pegawai_tambah' ){
	
 	if(isset($_POST['pegawai'])){
	
	// Array ( [nama] => 123 [kelompok_bidang] => 1 [pangkat] => 123 [jabatan] => 123 [tanggal] => 29-03-2015 [email] => 123 [id] => 1 [pegawai] => Tambahkan ) 

	 	$kelompok_bidang = $_POST['kelompok_bidang'];
		
	 	$nama_lengkap = $_POST['nama'];
		
		$pangkat = $_POST['pangkat'];
		
		$jabatan = $_POST['jabatan'];
		
		$tanggal_lahir = date("Y-m-d",strtotime($_POST['tanggal']));

		$email = $_POST['email'];
		
		if(isset($_FILES['file']['name'])){
		
		 	$foto = $libs->uploadImageToFolder('../../../images/content/organization/',$_FILES['file']);	//upload
		 	
			if(!empty($foto) or $foto != false){
				
				$struktur_organisasi->insertPegawai($kelompok_bidang,$nama_lengkap,$pangkat,$tanggal_lahir,$email,$foto,$jabatan); // method penyimpanan
				
				echo"<script> alert('Manambahkan File'); </script>";
			}else{
				
				echo"<script> alert('Gagal mengunggah file, harap ulangi sesuai format yang telah ditentukan'); </script>";
				
			}
			

		}
		
		echo"<script> window.history.back(); </script>";
	}
 
 }
 
if($model=='struktur_organisasi' AND $method=='hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
		
	$struktur_organisasi->deleteStrukturOrganisasi($id);

	 echo"<script> alert('Menghapus data'); </script>";
	 
	 header("location:".URL."struktur_organisasi");
}

if($model=='struktur_organisasi' AND $method=='pegawai_hapus'){

 	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$libs-> hapusGambarSpesific('../../../images/content/organization/', $id);
	
	$struktur_organisasi->deletePegawai($id);
	
	echo"<script> window.history.back(); </script>";
	
}


 // header("location:".URL."halaman");
 
 endif;
?>
<?php
if(isset($method)):
$aksi = URL."controller/users_control/users_control.php"; //  untuk eksekusi
?>
<script type='text/javascript'> 
function hapusAlert(id){
		var conBox = confirm("Anda yakin ingin menghapus data ini?");
		if(conBox){ 
			location.href="<?php echo $aksi."?model=users&method=hapus" ;?>&id="+ id;
		}else{
			return false;
		}
};

function nomor1(evt)
		{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))

		return false;
		return true;
		}
		
</script>
<?php

date_default_timezone_set('Asia/Jakarta');

echo "
		<div class='row-fluid'>
                    <div class='span12'>
                        <!-- BEGIN BASIC PORTLET-->
                        <div class='widget black'>
                            <div class='widget-title'>
                                <h4><i class='icon-reorder'></i> Users</h4>
							<div class='actions'>
                               
                            </div>
                            
                        </div>
						<div class='widget-body'>
";



switch($method){

default :

	echo "
	<a href='".URL."./users/tambah' class='btn btn-success'>Tambah User</a>
	<br/>
	<br/>
	<table id='example1' class='table table-bordered table-striped ' >
	<thead>
		<tr >
			<th>No</th>
			<th>Username</th>
			<th>Level</th>
			<th>Nama Lengkap</th>
			<th>NIP</th>
			<th>Email</th>
			<th>Blokir</th>
			<th>Tindakan</th>
		</tr>
		
	</thead>
	<tbody>
	"
		
		; // header tabel

	$users = $users->getUser();

	$i= 1;

	foreach($users as $users){
		if($i%2 == 0){ $warna = 'odd';}else{$warna = 'even';}
		echo "
		<tr class='$warna'>
			<td>".$i."</td>

			<td>".$users['username']."</td>

			<td>".$users['level']."</td>
		
			<td>".$users['nama_lengkap']."</td>
		
			<td>".$users['nip']."</td>
			
			<td>".$users['email']."</td>
		
			<td>".$users['blokir']."</td>
		
			<td> <a class='btn btn-danger' href=\"javascript: hapusAlert('".$users['username']."');\"> Hapus</a> </td>
	
		</tr>
		
		";
	$i++;
	}
	echo "
		</tbody>	
	</table>

	";

	break;

case "tambah":

	echo "
	<h3> Tambah User </h3>
	
		<form role='form' enctype='multipart/form-data' action='$aksi?model=users&method=tambah' method='POST' name='form' class='form-horizontal' />
			
				<div class='control-group'>
					
					<label class='control-label'>Username</label>

					<div class='controls'>
						<input type='text' name='username' id='judul' placeholder='Nama User' required>
					</div>
				
				</div>
				<div class='control-group'>
				
				<label class='control-label'>Password</label>
					<div class='controls'>
						<input type='password' class='form-control' name='password'  placeholder='Password' required />				
					</div>
				
				</div>
				<div class='control-group'>
					
					<label class='control-label'>Level</label>
					<div class='controls'>
						<select name='level' class='form-control input-lg'>
							<option value='user'>User</option>
							<option value='admin'>Admin</option>
						<select>
					</div>
					
				</div>
				<div class='control-group'>
					
					<label class='control-label'>Nama Lengkap</label>
					<div class='controls'>
						<input type='text' class='form-control input-lg' name='nama_lengkap' placeholder='Nama Lengkap' required />				
					</div>
					
				</div>
				<div class='control-group'>
					
					<label class='control-label'>Nomor Pegawai</label>
					<div class='controls'>
						<input type='text' class='form-control input-lg' name='nip' placeholder='Nomor Pegawai' required />				
					</div>
				</div>
				<div class='control-group'>
					
					<label class='control-label'>Email</label>
					<div class='controls'>
						<input type='text' class='form-control input-lg' name='email' placeholder='Email' required />				
					</div>				
				</div>
			<div >
				<button type='submit' class='btn btn-primary'>Submit</button>
			</div>
		</form>

	";

break;
	
case "edit":
	
	$id = $_GET['id'];
	
	// var_dump($id);
	 $users = $users->getUserByUsername($_GET['id']);
	 if($users[user]=='admin'){
		$admin ='selected';	
		
		}else{
		
		$umum ='selected';	
		
		}
		
	echo "
		<form role='form' enctype='multipart/form-data' action='$aksi?act=user&page=edit' method='POST' name='form' onsubmit='return cekData();' />
			<div class='box-body' >
				<div class='form-group'>
					
					<label for='exampleInputEmail1'>Nama User</label>
					<input type='text' class='form-control' name='nama' id='judul' placeholder='Nama User' value='$users[user]' required>
					<input type='hidden' class='form-control' name='id' id='judul' placeholder='Nama User' value='$id' required>
				
				</div>
				<div class='form-group'>
				
					<label>Password (kosongkan jika tidak ada perubahan password)</label>
					<input type='password' class='form-control' name='pwd' value=''  placeholder='Password'  />				
				
				</div>
				<div class='form-group'>
					
					<label >Level</label>
					
					<select name='level' class='form-control input-lg'>
						<option value='umum' $umum>Umum</option>
						<option value='Admin' $admin>Admin</option>
						<option value='Super' $admin>Super</option>
					<select>
					
				</div>
				<div class='form-group'>
					
					<label >Universitas</label>
					
					<input type='text' class='form-control input-lg' name='universitas' value='$users[universitas]' placeholder='Universitas' required />				
				
				</div>
				<div class='form-group'>
					
					<label >URL ex:(www.abc.ac.id)</label>
					
					<input type='text' class='form-control input-lg' name='url' value='$users[url]' placeholder='Alamat URL' required />				
				
				</div>
			</div><!-- /.box-body -->

			<div class='box-footer'>
				<button type='submit' class='btn btn-primary'>Submit</button>
			</div>
		</form>

	";

echo " 		</div>
		</div>
    </div>";//end of wrapper
break;
}
?>


<?php endif;?>
<?php 
// session_start();
if($_REQUEST['model']): // for secure single file
ob_start();
date_default_timezone_set('Asia/Jakarta');

require '../../libs/path.php';
require '../../model/class.php';

$model=$_GET['model'];

$method=$_GET['method'];
$model ;

// echo $method;
// echo $parameter;

if($model=='galeri' AND $method=='tambah' ){
	
 	if(isset($_POST['submit'])){
		
		$nama = $_POST['nama'];
		
		$nama = filter_var($nama, FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		// $isi = $libs->stringHtml($isi);

 		$link = $libs->changeLink($nama);

		// if(isset($_FILES['file'])){
		
		// $nama_baru = $libs->uploadImage($_FILES['file']);	
		
		// }

		$galeri->insertGaleri($nama,$link); // method penyimpanan

		echo"<script> alert('Menambah data'); </script>";
		
		 header("location:".URL."galeri");
	}
 
 // return false;
 }
 
if($model=='galeri' AND $method=='edit' ){
	
 	if(isset($_POST['submit'])){
		
		$nama = $_POST['nama'];
		$id = $_POST['id'];
		
		$nama = filter_var(trim($nama), FILTER_SANITIZE_MAGIC_QUOTES); // sanitasi 
		// $isi = $libs->stringHtml($isi);

 		$link = $libs->changeLink($nama);

		// if(isset($_FILES['file'])){
		
		// $nama_baru = $libs->uploadImage($_FILES['file']);	
		
		// }

		$galeri->updateGaleri($nama,$link,$id); // method penyimpanan

		echo"<script> alert('Mengubah data'); </script>";
		
		 header("location:".URL."galeri");
	}
 
 // return false;
 }
 
if($model=='galeri' AND $method=='file_tambah' ){
	
 	if(isset($_POST['submit'])){

	 	$id_galeri = $_POST['id'];
		
		$keterangan = $_POST['keterangan'];

		if(isset($_FILES['file'])){
		
		 	$nama_file = $libs->uploadImageToFolder('../../../images/content/gallery/',$_FILES['file']);	//upload
		 	
			$thumbnail = $libs->uploadImageToFolderThumbnail('../../../images/content/gallery/',$nama_file);	//thumbnail
			// uploadImageToFolderThumbnail($folder,$filename)
			
			if(!empty($nama_file) or $nama_file != false){
				
				$galeri->insetDetailGaleri($id_galeri,$keterangan,$nama_file); // method penyimpanan
				
				echo"<script> alert('Manambahkan File'); </script>";
			}else{
				
				echo"<script> alert('Gagal mengunggah file, harap ulangi sesuai format yang telah ditentukan'); </script>";
				
			}
			

		}
		
		echo"<script> window.history.back(); </script>";
	}
 
 }
 
if($model=='galeri' AND $method=='hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
		
	$galeri->deleteGaleri($id);

	 echo"<script> alert('Menghapus data'); </script>";
	 
	 header("location:".URL."galeri");
}

if($model=='galeri' AND $method=='file_hapus'){

	$id = filter_var($_GET['id'],FILTER_VALIDATE_INT);
	
	$data = $galeri->getGaleriDetilById($id);
	
	$libs-> hapusGambarSpesific("../../../images/content/gallery/", $data['nama_file']);
	$libs-> hapusGambarSpesific("../../../images/content/gallery/thumbnails/", $data['nama_file']);
	
	$galeri->deleteGaleriDetail($id);

	echo"<script> window.history.back(); </script>";

}


 // header("location:".URL."halaman");
 
 endif;
?>